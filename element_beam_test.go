/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestElementBeam(t *testing.T) {
	var cases = []string{
		`*ELEMENT_BEAM
$    eid     pid      n1      n2      n3     rt1     rr1     rt2     rr2   local
 5421224      76 1918392 1918393 1918793       0       0       0       0       2
 5421225      77 1918393 1918394 1918794       0       0       0       0       2
`}
	CheckCases(cases, t)
}
