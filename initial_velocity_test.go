/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestInitialVelocity(t *testing.T) {
	var cases = []string{
		`*INITIAL_VELOCITY_NODE
$      nid        vx        vy        vz       vxr       vyr       vzr      icid
         2 1.200E+00 2.300E+00 3.400E+00 4.500E+00 5.600E+00 6.700E+00         1
         3 2.200E+00 3.300E+00 4.400E+00 5.500E+00 5.600E+00 6.700E+00         1
         4 3.200E+00 4.300E+00 5.400E+00 6.500E+00 5.600E+00 6.700E+00         1
`}
	CheckCases(cases, t)
}
