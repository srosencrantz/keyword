/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"math"
)

type InitialStressShell struct {
	Id                  int64
	Nplane              int64
	Nthick              int64
	Nhisv               int64
	Ntensr              int64
	Large               int64
	Nthint              int64
	Nthhsv              int64
	Set                 bool
	IntegrationPoints   []*InitialStressShellIntegrationPoint
	ThermalHistoryLines []*InitialStressShellThermalHistoryLine
}

type InitialStressShellIntegrationPoint struct {
	StressLine   *InitialStressShellStressLine
	HistoryLines []*InitialStressShellHistoryLine
	TensorLines  []*InitialStressShellTensorLine
}

var initialStressShellCard = card{i10, i10, i10, i10d, i10d, i10d, i10d, i10d}

type InitialStressShellStressLine struct {
	T, Sigxx, Sigyy, Sigzz, Sigxy, Sigyz, Sigzx, Eps float64
}

var initialStressShellStressCard = card{f10, f10, f10, f10, f10, f10, f10, f10}

var initialStressShellLargeStressCards = []card{card{f16, f16, f16, f16, f16}, card{f16, f16, f16}}

type InitialStressShellHistoryLine struct {
	Hisv1, Hisv2, Hisv3, Hisv4, Hisv5, Hisv6, Hisv7, Hisv8 float64
}

var initialStressShellHistoryCard = card{f10, f10, f10, f10, f10, f10, f10, f10}

var initialStressShellLargeHistoryCard = card{f16, f16, f16, f16, f16}

type InitialStressShellTensorLine struct {
	Tenxx, Tenyy, Tenzz, Tenxy, Tenyz, Tenzx float64
}

var initialStressShellTensorCard = card{f10, f10, f10, f10, f10, f10}

var initialStressShellLargeTensorCards = []card{card{f16, f16, f16, f16, f16}, card{f16}}

type InitialStressShellThermalHistoryLine struct {
	Thhsv1, Thhsv2, Thhsv3, Thhsv4, Thhsv5 float64
}

var initialStressShellThermalHistoryCard = card{f16, f16, f16, f16, f16}

type InitialStressShellList []*InitialStressShell

func (l *InitialStressShellList) parseElement(head []byte, p *parser) error {
	n := &InitialStressShell{}
	*l = append(*l, n)
	if err := p.parseCardToStruct(initialStressShellCard, 0, n); err != nil {
		return err
	}
	if bytes.Contains(head, []byte("_SET")) {
		n.Set = true
	}
	for i := int64(0); i < n.Nplane*n.Nthick; i++ {
		ip := &InitialStressShellIntegrationPoint{}
		n.IntegrationPoints = append(n.IntegrationPoints, ip)
		ip.StressLine = &InitialStressShellStressLine{}
		if n.Large == int64(0) {
			if err := p.parseCardToStruct(initialStressShellStressCard, 0, ip.StressLine); err != nil {
				return err
			}
			for j := 0; j < int(math.Ceil(float64(n.Nhisv)/float64(8.0))); j++ {
				hc := &InitialStressShellHistoryLine{}
				ip.HistoryLines = append(ip.HistoryLines, hc)
				if err := p.parseCardToStruct(initialStressShellHistoryCard, 0, hc); err != nil {
					return err
				}
			}
			for j := 0; j < int(math.Ceil(float64(n.Ntensr)/float64(6.0))); j++ {
				tc := &InitialStressShellTensorLine{}
				ip.TensorLines = append(ip.TensorLines, tc)
				if err := p.parseCardToStruct(initialStressShellTensorCard, 0, tc); err != nil {
					return err
				}
			}
		} else {
			if err := p.parseCardsToStruct(initialStressShellLargeStressCards, 0, ip.StressLine); err != nil {
				return err
			}
			for j := 0; j < int(math.Ceil(float64(n.Nhisv)/float64(5.0))); j++ {
				hc := &InitialStressShellHistoryLine{}
				ip.HistoryLines = append(ip.HistoryLines, hc)
				if err := p.parseCardToStruct(initialStressShellLargeHistoryCard, 0, hc); err != nil {
					return err
				}
			}
			for j := 0; j < int(math.Ceil(float64(n.Ntensr)/float64(6.0))); j++ {
				tc := &InitialStressShellTensorLine{}
				ip.TensorLines = append(ip.TensorLines, tc)
				if err := p.parseCardsToStruct(initialStressShellLargeTensorCards, 0, tc); err != nil {
					return err
				}
			}
		}
	}
	// loop through thermal records
	for i := 0; (i < int(math.Ceil(float64(n.Nthint*n.Nthhsv)/float64(5.0)))) && (n.Large == int64(1)); i++ {
		th := &InitialStressShellThermalHistoryLine{}
		n.ThermalHistoryLines = append(n.ThermalHistoryLines, th)
		if err := p.parseCardToStruct(initialStressShellThermalHistoryCard, 0, th); err != nil {
			return err
		}
	}
	return nil
}

func (l *InitialStressShellList) dumpKeyword(d *dumper) error {
	first := true
	set := false
	elem := false
	for _, n := range *l {
		if n.Set {
			set = true
		}
		if !n.Set {
			elem = true
		}
		if set && elem {
			break
		}
	}
	for set || elem {
		if elem {
			if err := d.dumpLine("*INITIAL_STRESS_SHELL"); err != nil {
				return err
			}
		} else {
			if err := d.dumpLine("*INITIAL_STRESS_SHELL_SET"); err != nil {
				return err
			}
		}
		for _, n := range *l {
			if (elem && !n.Set) || (set && !elem && n.Set) {
				if first {
					if elem {
						if err := d.dumpLine("$      eid    nplane    nthick     nhisv    ntensr     large    nthint    nthhsv"); err != nil {
							return err
						}
					} else {
						if err := d.dumpLine("$      sid    nplane    nthick     nhisv    ntensr     large    nthint    nthhsv"); err != nil {
							return err
						}
					}
				}
				if err := d.dumpCardFromStruct(initialStressShellCard, 0, n); err != nil {
					return err
				}
				for _, ip := range n.IntegrationPoints {
					if n.Large == int64(0) {
						if first {
							if err := d.dumpLine("$        t     sigxx     sigyy     sigzz     sigxy     sigyz     sigzx       eps"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressShellStressCard, 0, ip.StressLine); err != nil {
							return err
						}

						for _, hl := range ip.HistoryLines {
							if first {
								if err := d.dumpLine("$    hisv1     hisv2     hisv3     hisv4     hisv5     hisv6     hisv7     hisv8"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressShellHistoryCard, 0, hl); err != nil {
								return err
							}
						}
						for _, tl := range ip.TensorLines {
							if first {
								if err := d.dumpLine("$    tenxx     tenyy     tenzz     tenxy     tenyz     tenzx"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressShellTensorCard, 0, tl); err != nil {
								return err
							}
						}
					} else {
						if first {
							if err := d.dumpLine("$        t     sigxx     sigyy     sigzz     sigxy"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressShellLargeStressCards[0], 0, ip.StressLine); err != nil {
							return err
						}
						if first {
							if err := d.dumpLine("$    sigyz     sigzx       eps"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressShellLargeStressCards[1], 5, ip.StressLine); err != nil {
							return err
						}

						for _, hl := range ip.HistoryLines {
							if first {
								if err := d.dumpLine("$    hisv1     hisv2     hisv3     hisv4     hisv5"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressShellLargeHistoryCard, 0, hl); err != nil {
								return err
							}
						}
						for _, tl := range ip.TensorLines {
							if first {
								if err := d.dumpLine("$    tenxx     tenyy     tenzz     tenxy     tenyz"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressShellLargeTensorCards[0], 0, tl); err != nil {
								return err
							}
							if first {
								if err := d.dumpLine("$    tenzx"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressShellLargeTensorCards[1], 5, tl); err != nil {
								return err
							}
						}
					}
				}
				// loop through thermal records
				for _, tl := range n.ThermalHistoryLines {
					if first {
						if err := d.dumpLine("$   thhsv1    thhsv2    thhsv3    thhsv4    thhsv5"); err != nil {
							return err
						}
					}
					if err := d.dumpCardFromStruct(initialStressShellThermalHistoryCard, 0, tl); err != nil {
						return err
					}
				}
				first = false
			}
		}
		if elem {
			elem = false
			first = true
		} else {
			set = false
		}
	}
	return nil
}

func (l *InitialStressShellList) clear() {
	*l = nil
}
