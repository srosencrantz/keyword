/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type ConstrainedGeneralizedWeldList []*ConstrainedGeneralizedWeld

type ConstrainedGeneralizedWeld struct {
	Id                int64
	Nsid              int64
	Cid               int64
	Filter            int64
	Window            float64
	Npr               int64
	Nprt              int64
	Spot              *SpotLine
	Fillet            *FilletLine
	Butt              *ButtLine
	CrossFillet       *CrossFilletLine
	NodePairs         []*NodePairLine
	CombinedWeldLines []*CombinedWeldLine
}

var idCard = card{i10}
var cgwCard = card{i10, i10, i10, f10, i10, i10}

// type SpotLine struct {
// 	Tfail float64
// 	Epsf  float64
// 	Sn    float64
// 	Ss    float64
// 	N     float64
// 	M     float64
// }
// var spotCard = card{f10d, f10d, f10d, f10d, f10d, f10d}

type SpotLine struct {
	Tfail float64
	Epsf  string
	Sn    string
	Ss    string
	N     string
	M     string
}

var spotCard = card{f10, stringF{10}, stringF{10}, stringF{10}, stringF{10}, stringF{10}}

type FilletLine struct {
	Tfail float64
	Epsf  float64
	Sigf  float64
	Beta  float64
	L     float64
	W     float64
	A     float64
	Alpha float64
}

var filletCard = card{f10, f10, f10, f10, f10, f10, f10, f10}

type ButtLine struct {
	Tfail float64
	Epsf  float64
	Sigy  float64
	Beta  float64
	L     float64
	D     float64
}

var buttCard = card{f10, f10, f10, f10, f10, f10}

type CrossFilletLine struct {
	Tfail float64
	Epsf  float64
	Sigy  float64
	Beta  float64
	L     float64
	W     float64
	A     float64
	Alpha float64
}

var crossFilletCard = card{f10, f10, f10, f10, f10, f10, f10, f10}

// Used with CROSS_FILLET read in Npr of the following
type NodePairLine struct {
	Nodea int64
	Nodeb int64
	Ncid  int64
}

var nodePairCard = card{f10, f10, f10}

type CombinedWeldLine struct {
	Tfail float64
	Epsf  float64
	Sigy  float64
	Beta  float64
	L     float64
	W     float64
	A     float64
	Alpha float64
	Nodea int64
	Nodeb int64
	Ncid  int64
	Wtyp  int64
}

var combinedWeldLineCards = []card{{f10, f10, f10, f10, f10, f10, f10, f10}, {i10, i10, i10, i10}}

func (l *ConstrainedGeneralizedWeldList) parseElement(head []byte, p *parser) error {
	n := &ConstrainedGeneralizedWeld{}
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_ID")) {
		if err := p.parseCardToStruct(idCard, 0, n); err != nil {
			return err
		}
	}
	if err := p.parseCardToStruct(cgwCard, 1, n); err != nil {
		return err
	}
	switch {
	case bytes.Contains(head, []byte("_SPOT")):
		n.Spot = &SpotLine{}
		if err := p.parseCardToStruct(spotCard, 0, n.Spot); err != nil {
			return err
		}
	case bytes.Contains(head, []byte("_FILLET")):
		n.Fillet = &FilletLine{}
		if err := p.parseCardToStruct(filletCard, 0, n.Fillet); err != nil {
			return err
		}
	case bytes.Contains(head, []byte("_BUTT")):
		n.Butt = &ButtLine{}
		if err := p.parseCardToStruct(buttCard, 0, n.Butt); err != nil {
			return err
		}
	case bytes.Contains(head, []byte("_CROSS_FILLET")):
		n.CrossFillet = &CrossFilletLine{}
		if err := p.parseCardToStruct(crossFilletCard, 0, n.CrossFillet); err != nil {
			return err
		}
		n.NodePairs = make([]*NodePairLine, 0)
		for {
			m := &NodePairLine{}
			if err := p.parseCardToStruct(nodePairCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.NodePairs = append(n.NodePairs, m)
		}
	case bytes.Contains(head, []byte("_COMBINED")):
		n.CombinedWeldLines = make([]*CombinedWeldLine, 0)
		for {
			m := &CombinedWeldLine{}
			if err := p.parseCardsToStruct(combinedWeldLineCards, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.CombinedWeldLines = append(n.CombinedWeldLines, m)
		}
	}
	return nil
}

func (l *ConstrainedGeneralizedWeldList) dumpKeyword(d *dumper) error {
	first := true
	for _, n := range *l {
		head := []byte("*CONSTRAINED_GENERALIZED_WELD")
		switch {
		case n.Spot != nil:
			head = append(head, []byte("_SPOT")...)
		case n.Fillet != nil:
			head = append(head, []byte("_FILLET")...)
		case n.Butt != nil:
			head = append(head, []byte("_BUTT")...)
		case n.CrossFillet != nil:
			head = append(head, []byte("_CROSS_FILLET")...)
		case n.CombinedWeldLines != nil:
			head = append(head, []byte("_COMBINED")...)
		}
		if n.Id != 0 {
			head = append(head, []byte("_ID")...)
		}
		if err := d.dumpLine(string(head)); err != nil {
			return err
		}

		if n.Id != 0 {
			if first {
				if err := d.dumpLine("$      wid"); err != nil {
					return err
				}
			}
			if err := d.dumpCardFromStruct(idCard, 0, n); err != nil {
				return err
			}
		}
		if first {
			if err := d.dumpLine("$     nsid       cid    filter    window       npr      nprt"); err != nil {
				return err
			}
		}
		if err := d.dumpCardFromStruct(cgwCard, 1, n); err != nil {
			return err
		}

		switch {
		case n.Spot != nil:
			if first {
				if err := d.dumpLine("$    tfail      epsf        sn        ss         n         m"); err != nil {
					return err
				}
			}
			if err := d.dumpCardFromStruct(spotCard, 0, n.Spot); err != nil {
				return err
			}
		case n.Fillet != nil:
			if first {
				if err := d.dumpLine("$    tfail      epsf      sigf      beta         l         w         a     alpha"); err != nil {
					return err
				}
			}
			if err := d.dumpCardFromStruct(filletCard, 0, n.Fillet); err != nil {
				return err
			}
		case n.Butt != nil:
			if first {
				if err := d.dumpLine("$    tfail      epsf      sigy      beta         l         d"); err != nil {
					return err
				}
			}
			if err := d.dumpCardFromStruct(buttCard, 0, n.Butt); err != nil {
				return err
			}
		case n.CrossFillet != nil:
			if first {
				if err := d.dumpLine("$    tfail      epsf      sigy      beta         l         w         a     alpha"); err != nil {
					return err
				}
			}
			if err := d.dumpCardFromStruct(crossFilletCard, 0, n.CrossFillet); err != nil {
				return err
			}
			for i, m := range n.NodePairs {
				if first && i == 0 {
					if err := d.dumpLine("$    nodea     nodeb      ncid"); err != nil {
						return err
					}
				}
				if err := d.dumpCardFromStruct(nodePairCard, 0, m); err != nil {
					return err
				}
			}
		case n.CombinedWeldLines != nil:
			for i, m := range n.CombinedWeldLines {
				if first && i == 0 {
					if err := d.dumpLine("$    tfail      epsf      sigy      beta         l         w         a     alpha"); err != nil {
						return err
					}
				}
				if err := d.dumpCardFromStruct(combinedWeldLineCards[0], 0, m); err != nil {
					return err
				}
				if first && i == 0 {
					if err := d.dumpLine("$    nodea     nodeb      ncid      wtyp"); err != nil {
						return err
					}
				}
				if err := d.dumpCardFromStruct(combinedWeldLineCards[1], 8, m); err != nil {
					return err
				}
			}
		}
		first = false
	}
	return nil
}

func (l *ConstrainedGeneralizedWeldList) clear() {
	*l = nil
}
