/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

// ELEMENT_SOLID

type ElementSolid struct {
	Eid int64
	Pid int64
	N   [8]int64
}

func (e *ElementSolid) IsTet() bool {
	if e.N[3] == e.N[4] && e.N[3] == e.N[5] && e.N[3] == e.N[6] && e.N[3] == e.N[7] {
		return true
	} else {
		return false
	}
}

func (e *ElementSolid) IsPent() bool {
	if e.N[4] == e.N[5] && e.N[6] == e.N[7] {
		return true
	} else {
		return false
	}
}

var elementSolidCard = card{i8, i8, arrIntF{8, intDflt(0)}}

type ElementSolidList []*ElementSolid

func (l *ElementSolidList) parseElement(head []byte, p *parser) error {
	e := &ElementSolid{}
	*l = append(*l, e)
	err := p.parseCardToStruct(elementSolidCard, 0, e)
	return err
}

func (l *ElementSolidList) dumpKeyword(d *dumper) error {
	var err error
	if err := d.dumpLine("*ELEMENT_SOLID"); err != nil {
		return err
	}
	if err := d.dumpLine("$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8"); err != nil {
		return err
	}
	for _, e := range *l {
		if err != nil {
			break
		}
		if err = d.dumpCard(elementSolidCard, e.Eid, e.Pid, e.N[:]); err != nil {
			break
		}
	}
	return err
}

func (l *ElementSolidList) clear() {
	*l = nil
}
