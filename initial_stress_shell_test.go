/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestInitialStressShell(t *testing.T) {
	var cases = []string{
		`*INITIAL_STRESS_SHELL
$      eid    nplane    nthick     nhisv    ntensr     large    nthint    nthhsv
    100000         1         3
$        t     sigxx     sigyy     sigzz     sigxy     sigyz     sigzx       eps
 0.000E+00 3.038E+02 2.982E+02-1.630E+01 5.380E+02 2.696E+02-6.385E+01 0.000E+00
$        t     sigxx     sigyy     sigzz     sigxy     sigyz     sigzx       eps
-7.746E-01 6.852E+00-6.205E+02-3.969E+01 3.890E+02 1.660E+02-1.372E+02 0.000E+00
$        t     sigxx     sigyy     sigzz     sigxy     sigyz     sigzx       eps
 7.746E-01 6.010E+02 1.217E+03 7.063E+00 6.871E+02 3.732E+02 9.447E+00 0.000E+00
    100001         1         3
 0.000E+00-1.749E+03-3.899E+02-6.857E+01-6.624E+02-2.109E+02-3.242E+02 0.000E+00
-7.746E-01-2.222E+03-1.098E+03-9.435E+01-7.000E+02-2.746E+02-4.246E+02 0.000E+00
 7.746E-01-1.277E+03 3.186E+02-4.277E+01-6.249E+02-1.471E+02-2.236E+02 0.000E+00
    100002         1         3
 0.000E+00-4.359E+02 4.833E+02-7.267E+00 1.615E+02 1.359E+02-9.420E+01 0.000E+00
-7.746E-01-7.156E+02 3.476E+02-2.207E+01 9.804E+01 1.123E+02-1.569E+02 0.000E+00
 7.746E-01-1.561E+02 6.191E+02 7.534E+00 2.250E+02 1.596E+02-3.147E+01 0.000E+00
`}
	CheckCases(cases, t)
}
