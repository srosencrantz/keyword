/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestBoundaryTemperature(t *testing.T) {
	var cases = []string{
		`*BOUNDARY_TEMPERATURE_NODE
$      nid      lcid     cmult       loc
         1         2 1.000E+00         0
         2         3 1.000E+00        -1
         3         4 1.000E+00         1
*BOUNDARY_TEMPERATURE_SET
$      sid      lcid     cmult       loc
         4         5 1.000E+00         0
         5         6 3.200E+00        -1
         6         7-1.230E+00         1
`}

	CheckCases(cases, t)
}
