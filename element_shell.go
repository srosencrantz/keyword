/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

// ELEMENT_SHELL

type ElementShell struct {
	Eid int64
	Pid int64
	N   [8]int64
}

func (e *ElementShell) IsTri() bool {
	if e.N[3] == e.N[4] {
		return true
	} else {
		return false
	}
}

var elementShellCard = card{i8, i8, arrIntF{8, intDflt(0)}}

type ElementShellList []*ElementShell

func (l *ElementShellList) parseElement(head []byte, p *parser) error {
	e := &ElementShell{}
	*l = append(*l, e)
	err := p.parseCardToStruct(elementShellCard, 0, e)
	return err
}

func (l *ElementShellList) dumpKeyword(d *dumper) error {
	var err error
	if err := d.dumpLine("*ELEMENT_SHELL"); err != nil {
		return err
	}
	if err := d.dumpLine("$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8"); err != nil {
		return err
	}
	for _, e := range *l {
		if err != nil {
			break
		}
		if err = d.dumpCard(elementShellCard, e.Eid, e.Pid, e.N[:]); err != nil {
			break
		}
	}
	return err
}

func (l *ElementShellList) clear() {
	*l = nil
}

// ELEMENT_SHELL_THICKNESS
type ElementShellThickness struct {
	Eid  int64
	Pid  int64
	N    [8]int64
	Thic [4]float64
	Psi  float64
}

func (e *ElementShellThickness) IsTri() bool {
	if e.N[3] == e.N[4] {
		return true
	} else {
		return false
	}
}

var elementShellThicknessCards = []card{elementShellCard, {arrFloatF{16, floatDflt(0)}, f16}}

type ElementShellThicknessList []*ElementShellThickness

func (l *ElementShellThicknessList) parseElement(head []byte, p *parser) error {
	e := &ElementShellThickness{}
	*l = append(*l, e)
	err := p.parseCardsToStruct(elementShellThicknessCards, 0, e)
	return err
}

func (l *ElementShellThicknessList) dumpKeyword(d *dumper) error {
	var err error
	if err := d.dumpLine("*ELEMENT_SHELL_THICKNESS"); err != nil {
		return err
	}
	for i, e := range *l {
		if i == 0 {
			if err := d.dumpLine("$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8"); err != nil {
				return err
			}
		}
		if err = d.dumpCardFromStruct(elementShellThicknessCards[0], 0, e); err != nil {
			break
		}
		if i == 0 {
			if err := d.dumpLine("$          thic1           thic2           thic3           thic4             psi"); err != nil {
				return err
			}
		}
		if err = d.dumpCardFromStruct(elementShellThicknessCards[1], 3, e); err != nil {
			break
		}
	}
	return err
}

func (l *ElementShellThicknessList) clear() {
	*l = nil
}
