/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestMatRigid(t *testing.T) {
	var cases = []string{
		`*MAT_RIGID
$       id        ro         e        pr         n    couple         m  alias/re
         1 7.200E-06 9.000E+12 3.000E-01
$      cm0      con1      con2

$lc0 or a1        a2        a3        v1        v2        v3

*MAT_RIGID_TITLE
This is a mat rigid title
$       id        ro         e        pr         n    couple         m  alias/re
         1 7.200E-06 9.000E+12 3.000E-01 1.000E+00 2.000E+00 3.000E+00 4.000E+00
$      cm0      con1      con2
 1.000E+00 2.000E+00 3.000E+00
$lc0 or a1        a2        a3        v1        v2        v3
 4.000E+00 5.000E+00 6.000E+00 7.000E+00 8.000E+00 9.000E+00
`}
	CheckCases(cases, t)
}
