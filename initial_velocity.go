/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

type InitialVelocityNode struct {
	Nid                       int64
	Vx, Vy, Vz, Vxr, Vyr, Vzr float64
	Icid                      int64
}

var initialVelocityNodeCard = card{i10, f10, f10, f10, f10, f10, f10, i10}

type InitialVelocityNodeList []*InitialVelocityNode

func (l *InitialVelocityNodeList) parseElement(head []byte, p *parser) error {
	n := &InitialVelocityNode{}
	*l = append(*l, n)
	return p.parseCardToStruct(initialVelocityNodeCard, 0, n)
}

func (l *InitialVelocityNodeList) dumpKeyword(d *dumper) error {
	if err := d.dumpLine("*INITIAL_VELOCITY_NODE"); err != nil {
		return err
	}
	if err := d.dumpLine("$      nid        vx        vy        vz       vxr       vyr       vzr      icid"); err != nil {
		return err
	}
	for _, n := range *l {
		if err := d.dumpCardFromStruct(initialVelocityNodeCard, 0, n); err != nil {
			return err
		}
	}
	return nil
}

func (l *InitialVelocityNodeList) clear() {
	*l = nil
}
