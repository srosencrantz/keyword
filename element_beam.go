/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

// ELEMENT_BEAM

type ElementBeam struct {
	Eid int64
	Pid int64
	N   [3]int64
	Rr1 int64
	Rt1 int64
	Rr2 int64
	Rt2 int64
	Loc int64
}

var elementBeamCard = card{i8, i8, arrIntF{8, intDflt(0)}, i8, i8, i8, i8, i8}

type ElementBeamList []*ElementBeam

func (l *ElementBeamList) parseElement(head []byte, p *parser) error {
	e := &ElementBeam{}
	*l = append(*l, e)
	err := p.parseCardToStruct(elementBeamCard, 0, e)
	return err
}

func (l *ElementBeamList) dumpKeyword(d *dumper) error {

	if err := d.dumpLine("*ELEMENT_BEAM"); err != nil {
		return err
	}
	if err := d.dumpLine("$    eid     pid      n1      n2      n3     rt1     rr1     rt2     rr2   local"); err != nil {
		return err
	}
	var err error
	for _, e := range *l {

		if err != nil {
			break
		}
		if err = d.dumpCardFromStruct(elementBeamCard, 0, e); err != nil {
			break
		}
	}
	return err
}

func (l *ElementBeamList) clear() {
	*l = nil
}
