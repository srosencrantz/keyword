/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestSectionBeam(t *testing.T) {
	var cases = []string{
		`*SECTION_BEAM_TITLE
This is a section beam title
$    secid    elform      shrf   qr/irid       cst     scoor       nsm     setyp
         1         2
$        a       iss       itt         j        sa       ist
 1.000E-01 1.000E-01 1.000E-01 1.000E-01
*SECTION_BEAM
$    secid    elform      shrf   qr/irid       cst     scoor       nsm     setyp
         9        13
$        a       iss       itt         j        sa       ist
 1.282E+02 8.807E+04 3.667E+05 4.548E+05 6.409E+01 1.863E+04
`}
	CheckCases(cases, t)
}
