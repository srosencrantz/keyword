/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestSetPart(t *testing.T) {
	var cases = []string{
		`*SET_PART
$      sid       da1       da2       da3       da4      solv
         1 1.000E+00 2.000E+00           3.000E+00MECH
$     pid1      pid2      pid3      pid4      pid5      pid6      pid7      pid8
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_PART_LIST_TITLE
This is a set part list title
$      sid       da1       da2       da3       da4      solv
         1 1.000E+00 2.000E+00           3.000E+00MECH
$     pid1      pid2      pid3      pid4      pid5      pid6      pid7      pid8
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_PART_LIST_GENERATE
$      sid       da1       da2       da3       da4      solv
         1 1.000E+00 2.000E+00           3.000E+00MECH
$    b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_PART_COLUMN
$      sid       da1       da2       da3       da4      solv
         1 1.000E+00 2.000E+00           3.000E+00
$      pid        a1        a2        a3        a4
         1 1.000E+00 2.000E+00 3.000E+00 4.000E+00
         1 5.000E+00 6.000E+00 7.000E+00 8.000E+00
`}
	CheckCases(cases, t)
}
