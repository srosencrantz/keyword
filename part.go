/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import "bytes"

type Part struct {
	Title  string
	Pid    int64
	Secid  int64
	Mid    int64
	Eosid  int64
	Hgid   int64
	Grav   int64
	Adpopt int64
	Tmid   int64
	*Inertia
}

type Inertia struct {
	Xc, Yc, Zc                   float64
	Tm                           float64
	Ircs                         int64
	Nid                          int64
	Ixx, Ixy, Ixz, Iyy, Iyz, Izz float64
	Vtx, Vty, Vtz                float64
	Vrx, Vry, Vrz                float64
	Xl, Yl, Zl                   float64
	Xlip, Ylip, Zlip             float64
	Cid                          int64
}

var partCards = []card{title, {i10, i10, i10, i10d, i10d, i10d, i10d, i10d}}
var inertiaCards = []card{
	{f10, f10, f10, f10, i10d, i10d},
	{f10, f10, f10, f10, f10, f10},
	{f10, f10, f10, f10, f10, f10},
}
var ircsCard = card{f10, f10, f10, f10, f10, f10, i10}

type PartList []*Part

func (l *PartList) parseElement(head []byte, p *parser) error {
	n := &Part{}
	*l = append(*l, n)
	if err := p.parseCardsToStruct(partCards, 0, n); err != nil {
		return err
	}
	if bytes.Contains(head, []byte("_INERTIA")) {
		n.Inertia = &Inertia{}
		if err := p.parseCardsToStruct(inertiaCards, 0, n.Inertia); err != nil {
			return err
		}
		if n.Inertia.Ircs == 1 {
			if err := p.parseCardToStruct(ircsCard, 18, n.Inertia); err != nil {
				return err
			}
		}
	}
	return nil
}

func (l *PartList) dumpKeyword(d *dumper) error {
	for _, n := range *l {
		head := []byte("*PART")
		if n.Inertia != nil {
			head = append(head, []byte("_INERTIA")...)
		}
		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if err := d.dumpLine("$ title"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(partCards[0], 0, n); err != nil {
			return err
		}
		if err := d.dumpLine("$      pid     secid       mid     eosid      hgid      grav    adpopt      tmid"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(partCards[1], 1, n); err != nil {
			return err
		}
		if n.Inertia != nil {
			if err := d.dumpLine("$       xc        yc        zc        tm      ircs      node"); err != nil {
				return err
			}
			if err := d.dumpCardFromStruct(inertiaCards[0], 0, n.Inertia); err != nil {
				return err
			}
			if err := d.dumpLine("$      ixx       ixy       ixz       iyy       iyz        iz"); err != nil {
				return err
			}
			if err := d.dumpCardFromStruct(inertiaCards[1], 6, n.Inertia); err != nil {
				return err
			}
			if err := d.dumpLine("$      vtx       vty       vtz       vrx       vry         v"); err != nil {
				return err
			}
			if err := d.dumpCardFromStruct(inertiaCards[2], 12, n.Inertia); err != nil {
				return err
			}
			if n.Inertia.Ircs == 1 {
				if err := d.dumpLine("$       xl        yl        zl      xlip      ylip      zlip       cid"); err != nil {
					return err
				}
				if err := d.dumpCardFromStruct(ircsCard, 18, n.Inertia); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (l *PartList) clear() {
	*l = nil
}

type PartMap map[int64]*Part

func (l *PartList) CreatePartMap() PartMap {
	pm := make(PartMap)
	for _, partptr := range *l {
		pm[partptr.Pid] = partptr
	}
	return pm
}
