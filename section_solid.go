/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type SectionSolid struct {
	Title  string
	Secid  int64
	Elform int64
	Aet    int64
}

var sectionSolidCard = card{i10, i10, i10}

type SectionSolidList []*SectionSolid

func (l *SectionSolidList) parseElement(head []byte, p *parser) error {
	e := &SectionSolid{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, e); err != nil {
			return err
		}
	}
	return p.parseCardToStruct(sectionSolidCard, 1, e)
}

func (l *SectionSolidList) dumpKeyword(d *dumper) error {
	for _, e := range *l {
		var head []byte
		if e.Title == "" {
			head = []byte("*SECTION_SOLID")
		} else {
			head = []byte("*SECTION_SOLID_TITLE")
		}
		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if e.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, e); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$    secid    elform       aet"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(sectionSolidCard, 1, e); err != nil {
			return err
		}
	}
	return nil
}

func (l *SectionSolidList) clear() {
	*l = nil
}
