/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

type ConstrainedAdaptivity struct {
	Sn, Mn1, Mn2 int64
}

var constrainedAdaptivityCard = card{i10, i10, i10}

type ConstrainedAdaptivityList []*ConstrainedAdaptivity

func (l *ConstrainedAdaptivityList) parseElement(head []byte, p *parser) error {
	n := &ConstrainedAdaptivity{}
	*l = append(*l, n)
	return p.parseCardToStruct(constrainedAdaptivityCard, 0, n)
}
func (l *ConstrainedAdaptivityList) dumpKeyword(d *dumper) error {
	if err := d.dumpLine("*CONSTRAINED_ADAPTIVITY"); err != nil {
		return err
	}
	if err := d.dumpLine("$       sn       mn1       mn2"); err != nil {
		return err
	}
	for _, n := range *l {
		if err := d.dumpCardFromStruct(constrainedAdaptivityCard, 0, n); err != nil {
			return err
		}
	}
	return nil
}

func (l *ConstrainedAdaptivityList) clear() {
	*l = nil
}
