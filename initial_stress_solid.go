/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"math"
)

type InitialStressSolid struct {
	Id                  int64
	Nint                int64
	Nhisv               int64
	Large               int64
	Iveflg              int64
	Ialegp              int64
	Nthint              int64
	Nthhsv              int64
	Set                 bool
	IntegrationPoints   []*InitialStressSolidIntegrationPoint
	ThermalHistoryLines []*InitialStressSolidThermalHistoryLine
}

type InitialStressSolidIntegrationPoint struct {
	StressLine   *InitialStressSolidStressLine
	HistoryLines []*InitialStressSolidHistoryLine
	TensorLines  []*InitialStressSolidTensorLine
}

var initialStressSolidCard = card{i10, i10, i10, i10d, i10d, i10d, i10d, i10d}

type InitialStressSolidStressLine struct {
	Sigxx, Sigyy, Sigzz, Sigxy, Sigyz, Sigzx, Eps float64
}

var initialStressSolidStressCard = card{f10, f10, f10, f10, f10, f10, f10}

var initialStressSolidLargeStressCards = []card{card{f16, f16, f16, f16, f16}, card{f16, f16, f16}}

type InitialStressSolidHistoryLine struct {
	Hisv1, Hisv2, Hisv3, Hisv4, Hisv5, Hisv6, Hisv7, Hisv8 float64
}

var initialStressSolidHistoryCard = card{f10, f10, f10, f10, f10, f10, f10, f10}

var initialStressSolidLargeHistoryCard = card{f16, f16, f16, f16, f16}

type InitialStressSolidTensorLine struct {
	Tenxx, Tenyy, Tenzz, Tenxy, Tenyz, Tenzx float64
}

var initialStressSolidTensorCard = card{f10, f10, f10, f10, f10, f10}

var initialStressSolidLargeTensorCards = []card{card{f16, f16, f16, f16, f16}, card{f16}}

type InitialStressSolidThermalHistoryLine struct {
	Thhsv1, Thhsv2, Thhsv3, Thhsv4, Thhsv5 float64
}

var initialStressSolidThermalHistoryCard = card{f16, f16, f16, f16, f16}

type InitialStressSolidList []*InitialStressSolid

func (l *InitialStressSolidList) parseElement(head []byte, p *parser) error {
	n := &InitialStressSolid{}
	*l = append(*l, n)
	if err := p.parseCardToStruct(initialStressSolidCard, 0, n); err != nil {
		return err
	}
	if bytes.Contains(head, []byte("_SET")) {
		n.Set = true
	}
	for i := int64(0); i < n.Nint; i++ {
		ip := &InitialStressSolidIntegrationPoint{}
		n.IntegrationPoints = append(n.IntegrationPoints, ip)
		ip.StressLine = &InitialStressSolidStressLine{}
		if n.Large == int64(0) {
			if err := p.parseCardToStruct(initialStressSolidStressCard, 0, ip.StressLine); err != nil {
				return err
			}
			for j := 0; j < int(math.Ceil(float64(n.Nhisv)/float64(8.0))); j++ {
				hc := &InitialStressSolidHistoryLine{}
				ip.HistoryLines = append(ip.HistoryLines, hc)
				if err := p.parseCardToStruct(initialStressSolidHistoryCard, 0, hc); err != nil {
					return err
				}
			}
		} else {
			if err := p.parseCardsToStruct(initialStressSolidLargeStressCards, 0, ip.StressLine); err != nil {
				return err
			}
			for j := 0; j < int(math.Ceil(float64(n.Nhisv)/float64(5.0))); j++ {
				hc := &InitialStressSolidHistoryLine{}
				ip.HistoryLines = append(ip.HistoryLines, hc)
				if err := p.parseCardToStruct(initialStressSolidLargeHistoryCard, 0, hc); err != nil {
					return err
				}
			}
		}
	}
	// loop through thermal records
	for i := 0; (i < int(math.Ceil(float64(n.Nthint*n.Nthhsv)/float64(5.0)))) && (n.Large == int64(1)); i++ {
		th := &InitialStressSolidThermalHistoryLine{}
		n.ThermalHistoryLines = append(n.ThermalHistoryLines, th)
		if err := p.parseCardToStruct(initialStressSolidThermalHistoryCard, 0, th); err != nil {
			return err
		}
	}
	return nil
}

func (l *InitialStressSolidList) dumpKeyword(d *dumper) error {
	first := true
	set := false
	elem := false
	for _, n := range *l {
		if n.Set {
			set = true
		}
		if !n.Set {
			elem = true
		}
		if set && elem {
			break
		}
	}
	for set || elem {
		if elem {
			if err := d.dumpLine("*INITIAL_STRESS_SOLID"); err != nil {
				return err
			}
		} else {
			if err := d.dumpLine("*INITIAL_STRESS_SOLID_SET"); err != nil {
				return err
			}
		}
		for _, n := range *l {
			if (elem && !n.Set) || (set && !elem && n.Set) {
				if first {
					if elem {
						if err := d.dumpLine("$      eid      nint     nhisv     large    iveflg    ialegp    nthint    nthhsv"); err != nil {
							return err
						}
					} else {
						if err := d.dumpLine("$      sid      nint     nhisv     large    iveflg    ialegp    nthint    nthhsv"); err != nil {
							return err
						}
					}
				}
				if err := d.dumpCardFromStruct(initialStressSolidCard, 0, n); err != nil {
					return err
				}
				for _, ip := range n.IntegrationPoints {
					if n.Large == int64(0) {
						if first {
							if err := d.dumpLine("$    sigxx     sigyy     sigzz     sigxy     sigyz     sigzx       eps"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressSolidStressCard, 0, ip.StressLine); err != nil {
							return err
						}

						for _, hl := range ip.HistoryLines {
							if first {
								if err := d.dumpLine("$    hisv1     hisv2     hisv3     hisv4     hisv5     hisv6     hisv7     hisv8"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressSolidHistoryCard, 0, hl); err != nil {
								return err
							}
						}
						for _, tl := range ip.TensorLines {
							if first {
								if err := d.dumpLine("$    tenxx     tenyy     tenzz     tenxy     tenyz     tenzx"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressSolidTensorCard, 0, tl); err != nil {
								return err
							}
						}
					} else {
						if first {
							if err := d.dumpLine("$        t     sigxx     sigyy     sigzz     sigxy"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressSolidLargeStressCards[0], 0, ip.StressLine); err != nil {
							return err
						}
						if first {
							if err := d.dumpLine("$    sigyz     sigzx       eps"); err != nil {
								return err
							}
						}
						if err := d.dumpCardFromStruct(initialStressSolidLargeStressCards[1], 5, ip.StressLine); err != nil {
							return err
						}

						for _, hl := range ip.HistoryLines {
							if first {
								if err := d.dumpLine("$    hisv1     hisv2     hisv3     hisv4     hisv5"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressSolidLargeHistoryCard, 0, hl); err != nil {
								return err
							}
						}
						for _, tl := range ip.TensorLines {
							if first {
								if err := d.dumpLine("$    tenxx     tenyy     tenzz     tenxy     tenyz"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressSolidLargeTensorCards[0], 0, tl); err != nil {
								return err
							}
							if first {
								if err := d.dumpLine("$    tenzx"); err != nil {
									return err
								}
							}
							if err := d.dumpCardFromStruct(initialStressSolidLargeTensorCards[1], 5, tl); err != nil {
								return err
							}
						}
					}
				}
				// loop through thermal records
				for _, tl := range n.ThermalHistoryLines {
					if first {
						if err := d.dumpLine("$   thhsv1    thhsv2    thhsv3    thhsv4    thhsv5"); err != nil {
							return err
						}
					}
					if err := d.dumpCardFromStruct(initialStressSolidThermalHistoryCard, 0, tl); err != nil {
						return err
					}
				}
				first = false
			}
		}
		if elem {
			elem = false
			first = true
		} else {
			set = false
		}
	}
	return nil
}

func (l *InitialStressSolidList) clear() {
	*l = nil
}
