/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestSectionShell(t *testing.T) {
	var cases = []string{
		`*SECTION_SHELL_TITLE
This is a section shell title
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
         1         2
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
 1.000E-01 1.000E-01 1.000E-01 1.000E-01                                       0
*SECTION_SHELL
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
         2         2                     1.000E+00 5.000E+00
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
 1.000E-01 2.000E-01 3.000E-01 4.000E-01                                       0
*SECTION_SHELL
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
         3         2           9.000E+00 1.000E+00 5.000E+00         1
$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp
 1.000E-01 1.000E-01 1.000E-01 1.000E-01                                       0
$       b1        b2        b3        b4        b5        b6        b7        b8
 1.000E-01 5.000E+00 1.050E+01 3.400E+00 4.200E+00 8.900E+00 8.910E+01 4.550E+01
 1.530E+01
`}
	CheckCases(cases, t)
}
