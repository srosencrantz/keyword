/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type DefineCurve struct {
	Title  string
	Lcid   int64
	Sidr   int64
	Sfa    float64
	Sfo    float64
	Offa   float64
	Offo   float64
	Dattyp int64
	Points []*DefineCurvePoint
}

type DefineCurvePoint struct {
	A float64
	O float64
}

var defineCurveCard = card{i10, i10d, floatF{10, floatDflt(1.0)}, floatF{10, floatDflt(1.0)}, f10d, f10d, i10d}
var defineCurvePointCard = card{f20d, f20d}

type DefineCurveList []*DefineCurve

func (l *DefineCurveList) parseElement(head []byte, p *parser) error {
	var err error
	n := &DefineCurve{}
	n.Points = make([]*DefineCurvePoint, 0)
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err = p.parseCardToStruct(title, 0, n); err != nil {
			return err
		}
	}
	if err = p.parseCardToStruct(defineCurveCard, 1, n); err != nil {
		return err
	}
	for {
		m := &DefineCurvePoint{}
		if err = p.parseCardToStruct(defineCurvePointCard, 0, m); err != nil {
			if err == unexpectedKeyword {
				err = nil
			}
			break
		}
		n.Points = append(n.Points, m)
	}
	return err
}

func (l *DefineCurveList) dumpKeyword(d *dumper) error {
	for _, n := range *l {
		var head []byte

		if n.Title == "" {
			head = []byte("*DEFINE_CURVE")
		} else {
			head = []byte("*DEFINE_CURVE_TITLE")
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if n.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, n); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$     lcid      sidr       sfa       sfo      offa      offo     datty"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(defineCurveCard, 1, n); err != nil {
			return err
		}
		if err := d.dumpLine("$                 a1                  o1"); err != nil {
			return err
		}
		for _, m := range n.Points {
			if err := d.dumpCardFromStruct(defineCurvePointCard, 0, m); err != nil {
				return err
			}
		}
	}
	return nil
}

func (l *DefineCurveList) clear() {
	*l = nil
}
