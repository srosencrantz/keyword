/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type MatRigid struct {
	Title    string
	Mid      int64
	R0       float64
	E        float64
	Pr       float64
	N        float64
	Couple   float64
	M        float64
	Alias_Re float64
	Cm0      float64
	Con1     float64
	Con2     float64
	Lco_A1   float64
	A2       float64
	A3       float64
	V1       float64
	V2       float64
	V3       float64
}

var matRigidCards = []card{
	{i10, f10, f10, f10, f10d, f10d, f10d, f10d},
	{f10d, f10d, f10d},
	{f10d, f10d, f10d, f10d, f10d, f10d}}

type MatRigidList []*MatRigid

func (l *MatRigidList) parseElement(head []byte, p *parser) error {
	var err error
	e := &MatRigid{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, e); err != nil {
			return err
		}
	}

	if err = p.parseCardsToStruct(matRigidCards, 1, e); err != nil {
		return err
	}
	return err
}

func (l *MatRigidList) dumpKeyword(d *dumper) error {
	var err error
	for _, e := range *l {
		var head []byte
		if e.Title == "" {
			head = []byte("*MAT_RIGID")
		} else {
			head = []byte("*MAT_RIGID_TITLE")
		}
		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if e.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, e); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$       id        ro         e        pr         n    couple         m  alias/re"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matRigidCards[0], 1, e); err != nil {
			break
		}
		if err := d.dumpLine("$      cm0      con1      con2"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matRigidCards[1], 9, e); err != nil {
			break
		}
		if err := d.dumpLine("$lc0 or a1        a2        a3        v1        v2        v3"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matRigidCards[2], 12, e); err != nil {
			break
		}
	}
	return err
}

func (l *MatRigidList) clear() {
	*l = nil
}
