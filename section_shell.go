/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"math"
)

type SectionShell struct {
	Title   string
	Secid   int64
	Elform  int64
	Shrf    float64
	Nip     float64
	Propt   float64
	Qr_Irid float64
	Icomp   int64
	Setyp   int64
	Thic    [4]float64
	Nloc    float64
	Marea   float64
	Idof    float64
	Edgset  int64
	Angles  []*SectionShellAngle
}

type SectionShellAngle struct {
	B [8]float64
}

var sectionShellCards = []card{{i10, i10, floatF{10, floatDflt(1.0)}, floatF{10, floatDflt(2)}, f10d, f10d, i10d, intF{10, intDflt(1)}},
	{arrFloatF{10, floatDflt(0)}, f10d, f10d, f10d, i10}}

var sectionShellAngleCard = card{arrFloatF{10, floatDflt(0)}}

type SectionShellList []*SectionShell

func (l *SectionShellList) parseElement(head []byte, p *parser) error {
	var err error
	e := &SectionShell{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err = p.parseCardToStruct(title, 0, e); err != nil {
			return err
		}
	}
	if err = p.parseCardsToStruct(sectionShellCards, 1, e); err != nil {
		return err
	}
	if e.Thic[0] != 0.0 && e.Thic[1] == 0.0 && e.Thic[2] == 0.0 && e.Thic[3] == 0.0 {
		for i := 1; i < 4; i++ {
			e.Thic[i] = e.Thic[0]
		}
	}

	if e.Icomp == 1 {
		e.Angles = make([]*SectionShellAngle, 0, 0)
		for i := 0; i < int(math.Ceil(e.Nip/float64(8.0))); i++ {
			b := &SectionShellAngle{}
			e.Angles = append(e.Angles, b)
			if err = p.parseCardToStruct(sectionShellAngleCard, 0, b); err != nil {
				break
			}
		}
	}
	return err
}

func (l *SectionShellList) dumpKeyword(d *dumper) error {
	var err error
	var head []byte
	for _, e := range *l {
		if e.Title == "" {
			head = []byte("*SECTION_SHELL")
		} else {
			head = []byte("*SECTION_SHELL_TITLE")
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if e.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, e); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(sectionShellCards[0], 1, e); err != nil {
			break
		}
		if err := d.dumpLine("$    secid    elform      shrf       nip     propt   qr/irid     icomp     setyp"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(sectionShellCards[1], 9, e); err != nil {
			break
		}
		for i, b := range e.Angles {
			if i == 0 {
				if err := d.dumpLine("$       b1        b2        b3        b4        b5        b6        b7        b8"); err != nil {
					return err
				}
			}
			if err = d.dumpCardFromStruct(sectionShellAngleCard, 0, b); err != nil {
				break
			}
		}
	}
	return err
}

func (l *SectionShellList) clear() {
	*l = nil
}
