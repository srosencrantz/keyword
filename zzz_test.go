/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

var testFile = `*KEYWORD
*TITLE
LS-DYNA keyword deck by LS-PrePost
`

func CheckCases(cases []string, t *testing.T) {
	for _, c := range cases {
		kwds, err := Parse(strings.NewReader(c), false)
		if err != nil {
			t.Fatalf("Couldn't parse: %s", err)
		}
		buf := &bytes.Buffer{}
		err = Dump(buf, kwds)
		if err != nil {
			t.Fatalf("Couldn't dump: %s", err)
		}
		if s := buf.String(); c != s {
			t.Errorf("Result doesn't match input, expected:\n%s\nGot:\n%s\n", c, s)
		}
		testFile = fmt.Sprint(testFile, c)
	}
}

func TestKeyword(t *testing.T) {
	testFile = fmt.Sprint(testFile, `*END
`)
	kwds, err := Parse(strings.NewReader(testFile), false)
	if err != nil {
		t.Fatal(err)
	}
	buf := &bytes.Buffer{}
	err = Dump(buf, kwds)
	if err != nil {
		t.Fatalf("Couldn't dump: %s", err)
	}
	// if s := buf.String(); testFile != s {
	// 	t.Errorf("Result doesn't match input, expected:\n%s\nGot:\n%s\n", testFile, s)
	// }
}

// func BenchmarkParse(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		file, err := os.Open("/Users/mattr/Downloads/largemodel/longonly2small.k")
// 		if err != nil {
// 			b.Fatal("Couldn't open file")
// 		}

// 		kwds, err := Parse(file)
// 		if err != nil {
// 			b.Fatal(err)
// 		}
// 		outf, ferr := os.Create(os.DevNull)
// 		if ferr != nil {
// 			b.Fatal(ferr)
// 		}
// 		if err = Dump(outf, kwds); err != nil {
// 			b.Fatal(err)
// 		}
// 	}
// }
