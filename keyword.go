/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"errors"
	"reflect"
)

// This struct represents and entire keyword file.
type KeywordFile struct {
	// Every type included as a member of this struct should implement
	// the keywordContainer interface.
	Keyword                      EmptyKeyword // Must be first.
	Title                        UnknownList
	Parameter                    UnknownList
	Part                         PartList
	PartAdaptiveFailure          UnknownList
	PartModes                    UnknownList
	PartSensor                   UnknownList
	PartMove                     UnknownList
	BoundarySpcNode              UnknownList
	DefineCurve                  DefineCurveList
	LoadRigidBody                LoadRigidBodyList
	LoadThermalVariableNode      LoadThermalVariableNodeList
	SectionBeam                  SectionBeamList
	SectionShell                 SectionShellList
	SectionSolid                 SectionSolidList
	SetNode                      SetNodeList
	SetShell                     SetShellList
	SetPart                      SetPartList
	BoundaryTemperature          BoundaryTemperatureList
	Node                         NodeList
	MatRigid                     MatRigidList
	MatPiecewiseLinearPlasticity MatPiecewiseLinearPlasticityList
	InitialVelocityNode          InitialVelocityNodeList
	InitialStressShell           InitialStressShellList
	InitialStressSolid           InitialStressSolidList
	ConstrainedAdaptivity        ConstrainedAdaptivityList
	ElementMass                  ElementMassList
	ElementBeam                  ElementBeamList
	ElementShell                 ElementShellList
	ElementShellThickness        ElementShellThicknessList
	InitialStressBeam            UnknownList
	InitialStrainShell           UnknownList
	InitialStrainSolid           UnknownList
	ElementSolid                 ElementSolidList
	ConstrainedGeneralizedWeld   ConstrainedGeneralizedWeldList
	Unknown                      UnknownList  // Unparsable sections will pass through verbatim.
	End                          EmptyKeyword // Must be last.
}

type keywordContainer interface {
	parseElement(head []byte, p *parser) error
	dumpKeyword(d *dumper) error
	clear()
}

type card []field

// Common field types.
var (
	i8    = intF{8, nil}
	i8d   = intF{8, intDflt(0)}
	i10   = intF{10, nil}
	i10d  = intF{10, intDflt(0)}
	f8    = floatF{8, nil}
	f8d   = floatF{8, floatDflt(0)}
	f10   = floatF{10, nil}
	f10d  = floatF{10, floatDflt(0)}
	f16   = floatF{16, nil}
	f16d  = floatF{16, floatDflt(0)}
	f20   = floatF{20, nil}
	f20d  = floatF{20, floatDflt(0)}
	title = card{stringF{80}}
)

// This field type is used to save unknown input keywords so they
// can be written back out unchanged when we write the file.
type UnknownList [][]byte

func (l *UnknownList) parseElement(head []byte, p *parser) error {
	u := append([]byte{}, head...)
	var err error
	for {
		head, err = p.getInKeywordLine()
		if err != nil {
			if err == unexpectedKeyword {
				err = nil
			}
			break
		}
		u = append(u, head...)
	}
	*l = append(*l, u)
	return err
}

func (l *UnknownList) dumpKeyword(d *dumper) error {
	for _, u := range *l {
		if err := d.dump(u); err != nil {
			return err
		}
	}
	return nil
}

func (l *UnknownList) clear() {
	*l = make([][]byte, 0)
}

// EmptyKeyword is used for keywords that have no content at all
// such as *KEYWORD and *END.
type EmptyKeyword string

func (l *EmptyKeyword) parseElement(head []byte, p *parser) error {
	if head[len(head)-1] == '\n' {
		head = head[:len(head)-1]
	}
	*l = EmptyKeyword(string(head))
	return nil
}

func (l *EmptyKeyword) dumpKeyword(d *dumper) error {
	return d.dumpLine(string(*l))
}

func (l *EmptyKeyword) clear() {
	*l = ""
}

func (k *KeywordFile) findContainer(head []byte) (keywordContainer, error) {
	// get the reduced keyword name
	name := byteToKeyword(head)

	// Now we look for the key in the keyword file struct.
	fileValue := reflect.ValueOf(k).Elem()

	if containerValue := fileValue.FieldByName(name); containerValue.IsValid() {
		return containerValue.Addr().Interface().(keywordContainer), nil
	}

	return nil, errors.New("unknown keyword")
}

func byteToKeyword(head []byte) string {
	// Trim the '*' and the newline at the end of the header line.
	end := len(head)
	if head[end-1] == '\n' {
		end--
	}
	head = head[1:end]

	// Cut off after the first space, everything after the first space
	// seems to be ignored, and is not part of the keyword.
	if idx := bytes.IndexByte(head, ' '); idx != -1 {
		head = head[:idx]
	}

	// Title case the whole thing.  This copies.
	head = bytes.Replace(head, []byte{'_'}, []byte{' '}, -1)
	head = bytes.Title(bytes.ToLower(head))

	// We eliminate the underscores because go variables are CamelCase.
	name := string(bytes.Replace(head, []byte{' '}, []byte{}, -1))
	return name
}

func (k *KeywordFile) CpContainer(inname string, k2 *KeywordFile) error {
	// get the reduced keyword name
	name := byteToKeyword([]byte(inname))

	// Now we look for the key in the keyword file struct.
	fileValue := reflect.ValueOf(k).Elem()
	fileValue2 := reflect.ValueOf(k2).Elem()

	if containerVal := fileValue.FieldByName(name); containerVal.IsValid() {
		if containerVal2 := fileValue2.FieldByName(name); containerVal2.IsValid() {
			containerVal2.Set(containerVal)
			return nil
		}
	}

	return errors.New("unknown keyword")
}

func (k *KeywordFile) Clear(name string) {
	container, err := k.findContainer([]byte(name))
	if err != nil {
		panic("Haven't implemented clearing unknown keywords.")
	}
	container.clear()
}
