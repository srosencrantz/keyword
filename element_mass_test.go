/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestElementMass(t *testing.T) {
	var cases = []string{
		`*ELEMENT_MASS
$    eid      id            mass     pid
       1       1 2.000000000E+00       1
       2       2 1.450000000E+07       2
       3       3 3.000000000E+00       3
*ELEMENT_MASS_NODE_SET
$    eid      id            mass     pid
       4       1 4.000000000E+00       1
       5       2 1.460110000E+07       2
       6       3 5.000000000E+00       3
*ELEMENT_MASS
$    eid      id            mass     pid
       7       1 6.000000000E+00       1
       8       2 1.471234567E+05       2
       9       3 7.000000000E+00       3
`}
	CheckCases(cases, t)
}
