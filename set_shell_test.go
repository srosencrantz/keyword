/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestSetShell(t *testing.T) {
	var cases = []string{
		`*SET_SHELL
$      sid       da1       da2       da3       da4
         1 1.000E+00 2.000E+00           3.000E+00
$     eid1      eid2      eid3      eid4      eid5      eid6      eid7      eid8
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_SHELL_LIST
$      sid       da1       da2       da3       da4
         1 1.000E+00 2.000E+00           3.000E+00
$     eid1      eid2      eid3      eid4      eid5      eid6      eid7      eid8
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_SHELL_LIST_GENERATE_TITLE
this is the set node list generate title
$      sid       da1       da2       da3       da4
         1 1.000E+00 2.000E+00           3.000E+00
$    b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end
         1         2         3         4         5         6         7         8
         1         2         3         4         5         6
*SET_SHELL_COLUMN
$      sid       da1       da2       da3       da4
         1 1.000E+00 2.000E+00           3.000E+00
$      eid        a1        a2        a3        a4
         1 1.000E+00 2.000E+00 3.000E+00 4.000E+00
         1 5.000E+00 6.000E+00 7.000E+00 8.000E+00
*SET_SHELL_GENERAL
$      sid       da1       da2       da3       da4
         1 1.000E+00 2.000E+00           3.000E+00
$   option        e1        e2        e3        e4        e5        e6        e7
PART               2         3         4         5         6         7         8
BOX                2         3         4         5         6
`}
	CheckCases(cases, t)
}
