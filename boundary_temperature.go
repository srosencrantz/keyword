/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type BoundaryTemperature struct {
	Id    int64
	Lcid  int64
	Cmult float64
	Loc   int64
	Node  bool
}

var boundaryTemperatureCard = card{i10, i10, f10, i10}

type BoundaryTemperatureList []*BoundaryTemperature

func (l *BoundaryTemperatureList) parseElement(head []byte, p *parser) error {
	n := &BoundaryTemperature{}
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_NODE")) {
		n.Node = true
		// Id is a node id
	} else {
		n.Node = false
		// Id is a set id
	}
	return p.parseCardToStruct(boundaryTemperatureCard, 0, n)
}

func (l *BoundaryTemperatureList) dumpKeyword(d *dumper) error {
	isNode := false
	isSet := false
	for _, n := range *l {
		if n.Node {
			isNode = true
		} else {
			isSet = true
		}
		if isNode && isSet {
			break
		}
	}

	// write Boundary_Temperature_Node records
	if isNode {
		if err := d.dumpLine("*BOUNDARY_TEMPERATURE_NODE"); err != nil {
			return err
		}
		if err := d.dumpLine("$      nid      lcid     cmult       loc"); err != nil {
			return err
		}
		for _, n := range *l {
			if n.Node {
				if err := d.dumpCardFromStruct(boundaryTemperatureCard, 0, n); err != nil {
					return err
				}
			}
		}
	}
	// write Boundary_Temperature_Set records
	if isSet {
		if err := d.dumpLine("*BOUNDARY_TEMPERATURE_SET"); err != nil {
			return err
		}
		if err := d.dumpLine("$      sid      lcid     cmult       loc"); err != nil {
			return err
		}
		for _, n := range *l {
			if !n.Node {
				if err := d.dumpCardFromStruct(boundaryTemperatureCard, 0, n); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (l *BoundaryTemperatureList) clear() {
	*l = nil
}
