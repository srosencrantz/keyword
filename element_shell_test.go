/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestElementShell(t *testing.T) {
	var cases = []string{
		`*ELEMENT_SHELL
$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8
       1       1       1      12      13       2                       1
       2       1       2      13      14       3
       3       1       3      14      15       4
*ELEMENT_SHELL_THICKNESS
$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8
      11       1       1      12      13       2                       1
$          thic1           thic2           thic3           thic4             psi
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 4.000000000E-02
      12       1       2      13      14       3
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      13       1       3      14      15       4
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      14       1       4      15      16       5
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      15       1       5      16      17       6
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      16       1       6      17      18       7
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      17       1       7      18      19       8
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      18       1       8      19      20       9
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
      19       1       9      20      21      10
 1.000000000E-01 1.000000000E-01 1.000000000E-01 1.000000000E-01 0.000000000E+00
`}
	CheckCases(cases, t)
}
