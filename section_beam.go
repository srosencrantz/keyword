/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type SectionBeam struct {
	Title   string
	Secid   int64
	Elform  int64
	Shrf    float64
	Qr_Irid float64
	Cst     float64
	Scoor   float64
	Nsm     float64
	Area    float64
	Iss     float64
	Itt     float64
	J       float64
	Sa      float64
	Ist     float64
}

var sectionBeamCards = []card{{i10, i10, floatF{10, floatDflt(1.0)}, floatF{10, floatDflt(2)}, f10d, f10d, f10d},
	{f10d, f10d, f10d, f10d, f10d, f10d}}

type SectionBeamList []*SectionBeam

func (l *SectionBeamList) parseElement(head []byte, p *parser) error {
	var err error
	e := &SectionBeam{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err = p.parseCardToStruct(title, 0, e); err != nil {
			return err
		}
	}
	if err = p.parseCardsToStruct(sectionBeamCards, 1, e); err != nil {
		return err
	}

	return err
}

func (l *SectionBeamList) dumpKeyword(d *dumper) error {
	var err error
	var head []byte
	for _, e := range *l {
		if e.Title == "" {
			head = []byte("*SECTION_BEAM")
		} else {
			head = []byte("*SECTION_BEAM_TITLE")
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if e.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, e); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$    secid    elform      shrf   qr/irid       cst     scoor       nsm     setyp"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(sectionBeamCards[0], 1, e); err != nil {
			break
		}
		if err := d.dumpLine("$        a       iss       itt         j        sa       ist"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(sectionBeamCards[1], 8, e); err != nil {
			break
		}
	}
	return err
}

func (l *SectionBeamList) clear() {
	*l = nil
}
