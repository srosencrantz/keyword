/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

type Node struct {
	Nid     int64
	X, Y, Z float64
	Tc, Rc  float64
}

var nodeCard = card{i8, f16, f16, f16, f8, f8}

type NodeList []*Node

func (l *NodeList) parseElement(head []byte, p *parser) error {
	n := &Node{}
	*l = append(*l, n)
	return p.parseCardToStruct(nodeCard, 0, n)
}
func (l *NodeList) dumpKeyword(d *dumper) error {
	if err := d.dumpLine("*NODE"); err != nil {
		return err
	}
	if err := d.dumpLine("$    nid               x               y               z      tc      rc"); err != nil {
		return err
	}
	for _, n := range *l {
		if err := d.dumpCardFromStruct(nodeCard, 0, n); err != nil {
			return err
		}
	}
	return nil
}

func (l *NodeList) clear() {
	*l = nil
}

type NodeMap map[int64]*Node

func (mp *NodeMap) GetNode(id int64) [3]float64 {
	m := *mp
	return [3]float64{m[id].X, m[id].Y, m[id].Z}
}

func (l *NodeList) CreateNodeMap() NodeMap {
	nodemap := make(NodeMap)
	for _, nodeptr := range *l {
		nodemap[nodeptr.Nid] = nodeptr
	}
	return nodemap
}
