/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

//import (
//)

type LoadThermalVariableNode struct {
	Id   int64
	Ts   float64
	Tb   float64
	Lcid int64
}

var loadThermalVariableNodeCard = card{i10, f10, f10, i10}

type LoadThermalVariableNodeList []*LoadThermalVariableNode

func (l *LoadThermalVariableNodeList) parseElement(head []byte, p *parser) error {
	n := &LoadThermalVariableNode{}
	*l = append(*l, n)
	return p.parseCardToStruct(loadThermalVariableNodeCard, 0, n)
}

func (l *LoadThermalVariableNodeList) dumpKeyword(d *dumper) error {
	// write Load_Thermal_Variable_Node records
	if err := d.dumpLine("*LOAD_THERMAL_VARIABLE_NODE"); err != nil {
		return err
	}
	if err := d.dumpLine("$      nid        ts        tb      lcid"); err != nil {
		return err
	}
	for _, n := range *l {
		if err := d.dumpCardFromStruct(loadThermalVariableNodeCard, 0, n); err != nil {
			return err
		}
	}
	return nil
}

func (l *LoadThermalVariableNodeList) clear() {
	*l = nil
}
