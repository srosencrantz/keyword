/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

type LoadRigidBody struct {
	Pid  int64
	Dof  int64
	Lcid int64
	Sf   float64
	Cid  int64
	M1   int64
	M2   int64
	M3   int64
}

var loadRigidBodyCard = card{i10, i10, i10, floatF{10, floatDflt(1.0)}, i10d, i10d, i10d, i10d}

type LoadRigidBodyList []*LoadRigidBody

func (l *LoadRigidBodyList) parseElement(head []byte, p *parser) error {
	e := &LoadRigidBody{}
	*l = append(*l, e)
	err := p.parseCardToStruct(loadRigidBodyCard, 0, e)
	return err
}

func (l *LoadRigidBodyList) dumpKeyword(d *dumper) error {
	var err error
	if err := d.dumpLine("*LOAD_RIGID_BODY"); err != nil {
		return err
	}
	if err := d.dumpLine("$      pid       dof      lcid        sf       cid        m1        m2        m3"); err != nil {
		return err
	}
	for _, e := range *l {
		if err != nil {
			break
		}
		if err = d.dumpCardFromStruct(loadRigidBodyCard, 0, e); err != nil {
			break
		}
	}
	return err
}

func (l *LoadRigidBodyList) clear() {
	*l = nil
}
