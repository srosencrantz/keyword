/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type SetNode struct {
	Title             string
	Sid               int64
	Da1               float64
	Da2               float64
	Da3               float64
	Da4               float64
	Solv              string
	BlankLines        []*SetNodeListLine
	ListLines         []*SetNodeListLine
	ColumnLines       []*SetNodeColumnLine
	ListGenerateLines []*SetNodeListLine
	GeneralLines      []*SetNodeGeneralLine
	ListSmoothLines   []*SetNodeListLine
}

var setNodeCard = card{i10, f10d, f10d, f10d, f10d, stringF{10}}

type SetNodeListLine struct {
	N [8]int64
}

var setNodeListLineCard = card{arrIntF{10, intDflt(0)}}

type SetNodeColumnLine struct {
	Nid int64
	A   [4]float64
}

var setNodeColumnLineCard = card{i10, arrFloatF{10, floatDflt(0.0)}}

type SetNodeGeneralLine struct {
	Option string
	E      [7]int64
}

var setNodeGeneralLineCard = card{stringF{10}, arrIntF{10, intDflt(0)}}

type SetNodeList []*SetNode

func (l *SetNodeList) parseElement(head []byte, p *parser) error {
	n := &SetNode{}
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, n); err != nil {
			return err
		}
	}
	if err := p.parseCardToStruct(setNodeCard, 1, n); err != nil {
		return err
	}
	switch {
	case bytes.Contains(head, []byte("_COLUMN")):
		n.ColumnLines = make([]*SetNodeColumnLine, 0)
		for {
			m := &SetNodeColumnLine{}
			if err := p.parseCardToStruct(setNodeColumnLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ColumnLines = append(n.ColumnLines, m)
		}
	case bytes.Contains(head, []byte("_GENERATE")):
		n.ListGenerateLines = make([]*SetNodeListLine, 0)
		for {
			m := &SetNodeListLine{}
			if err := p.parseCardToStruct(setNodeListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListGenerateLines = append(n.ListGenerateLines, m)
		}
	case bytes.Contains(head, []byte("_GENERAL")):
		n.GeneralLines = make([]*SetNodeGeneralLine, 0)
		for {
			m := &SetNodeGeneralLine{}
			if err := p.parseCardToStruct(setNodeGeneralLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.GeneralLines = append(n.GeneralLines, m)
		}
	case bytes.Contains(head, []byte("_SMOOTH")):
		n.ListSmoothLines = make([]*SetNodeListLine, 0)
		for {
			m := &SetNodeListLine{}
			if err := p.parseCardToStruct(setNodeListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListSmoothLines = append(n.ListSmoothLines, m)
		}
	case bytes.Contains(head, []byte("_LIST")):
		n.ListLines = make([]*SetNodeListLine, 0)
		for {
			m := &SetNodeListLine{}
			if err := p.parseCardToStruct(setNodeListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListLines = append(n.ListLines, m)
		}
	default:
		n.BlankLines = make([]*SetNodeListLine, 0)
		for {
			m := &SetNodeListLine{}
			if err := p.parseCardToStruct(setNodeListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.BlankLines = append(n.BlankLines, m)
		}
	}

}

func (l *SetNodeList) dumpKeyword(d *dumper) error {
	columnFirst := true
	listGenerateFirst := true
	generalFirst := true
	listSmoothFirst := true
	listFirst := true
	blankFirst := true
	for _, n := range *l {
		head := []byte("*SET_NODE")
		first := false
		switch {
		case n.ColumnLines != nil:
			head = append(head, []byte("_COLUMN")...)
			if columnFirst {
				first = true
			}
		case n.ListGenerateLines != nil:
			head = append(head, []byte("_LIST_GENERATE")...)
			if listGenerateFirst {
				first = true
			}
		case n.GeneralLines != nil:
			head = append(head, []byte("_GENERAL")...)
			if generalFirst {
				first = true
			}
		case n.ListSmoothLines != nil:
			head = append(head, []byte("_LIST_SMOOTH")...)
			if listSmoothFirst {
				first = true
			}
		case n.ListLines != nil:
			head = append(head, []byte("_LIST")...)
			if listFirst {
				first = true
			}
		case n.BlankLines != nil:
			if blankFirst {
				first = true
			}
		}
		if n.Title != "" {
			head = append(head, []byte("_TITLE")...)
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}

		if n.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, n); err != nil {
				return err
			}
		}

		if first {
			if err := d.dumpLine("$      sid       da1       da2       da3       da4      solv"); err != nil {
				return err
			}
		}
		if err := d.dumpCardFromStruct(setNodeCard, 1, n); err != nil {
			return err
		}

		switch {
		case n.ColumnLines != nil:
			if first {
				if err := d.dumpLine("$     nid1        a1        a2        a3        a4"); err != nil {
					return err
				}
			}
			for _, m := range n.ColumnLines {
				if err := d.dumpCardFromStruct(setNodeColumnLineCard, 0, m); err != nil {
					return err
				}
			}
			columnFirst = false
		case n.ListGenerateLines != nil:
			if first {
				if err := d.dumpLine("$    b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end"); err != nil {
					return err
				}
			}
			for _, m := range n.ListGenerateLines {
				if err := d.dumpCardFromStruct(setNodeListLineCard, 0, m); err != nil {
					return err
				}
			}
			listGenerateFirst = false
		case n.GeneralLines != nil:
			if first {
				if err := d.dumpLine("$   option        e1        e2        e3        e4        e5        e6        e7"); err != nil {
					return err
				}
			}
			for _, m := range n.GeneralLines {
				if err := d.dumpCardFromStruct(setNodeGeneralLineCard, 0, m); err != nil {
					return err
				}
			}
			generalFirst = false
		case n.ListSmoothLines != nil:
			if first {
				if err := d.dumpLine("$     nid1      nid2      nid3      nid4      nid5      nid6      nid7      nid8"); err != nil {
					return err
				}
			}
			for _, m := range n.ListSmoothLines {
				if err := d.dumpCardFromStruct(setNodeListLineCard, 0, m); err != nil {
					return err
				}
			}
			listSmoothFirst = false
		case n.ListLines != nil:
			if first {
				if err := d.dumpLine("$     nid1      nid2      nid3      nid4      nid5      nid6      nid7      nid8"); err != nil {
					return err
				}
			}
			for _, m := range n.ListLines {
				if err := d.dumpCardFromStruct(setNodeListLineCard, 0, m); err != nil {
					return err
				}
			}
			listFirst = false
		case n.BlankLines != nil:
			if first {
				if err := d.dumpLine("$     nid1      nid2      nid3      nid4      nid5      nid6      nid7      nid8"); err != nil {
					return err
				}
			}
			for _, m := range n.BlankLines {
				if err := d.dumpCardFromStruct(setNodeListLineCard, 0, m); err != nil {
					return err
				}
			}
			blankFirst = false
		}
	}
	return nil
}

func (l *SetNodeList) clear() {
	*l = nil
}
