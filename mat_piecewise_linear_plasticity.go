/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type MatPiecewiseLinearPlasticity struct {
	Title string
	Mid   int64
	R0    float64
	E     float64
	Pr    float64
	Sigy  float64
	Etan  float64
	Fail  float64
	Tdel  float64
	C     float64
	P     float64
	Lcss  float64
	Lcsr  float64
	Vp    float64
	Lcf   float64
	Eps   [8]float64
	Es    [8]float64
}

var matPiecewiseLinearPlasticityCards = []card{
	{i10, f10, f10, f10, f10, f10d, floatF{10, floatDflt(10E+20)}, f10d},
	{f10d, f10d, f10d, f10d, f10d, f10d},
	{arrFloatF{10, floatDflt(0)}},
	{arrFloatF{10, floatDflt(0)}}}

type MatPiecewiseLinearPlasticityList []*MatPiecewiseLinearPlasticity

func (l *MatPiecewiseLinearPlasticityList) parseElement(head []byte, p *parser) error {
	var err error
	e := &MatPiecewiseLinearPlasticity{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, e); err != nil {
			return err
		}
	}

	if err = p.parseCardsToStruct(matPiecewiseLinearPlasticityCards, 1, e); err != nil {
		return err
	}
	return err
}

func (l *MatPiecewiseLinearPlasticityList) dumpKeyword(d *dumper) error {
	var err error
	for _, e := range *l {
		var head []byte
		if e.Title == "" {
			head = []byte("*MAT_PIECEWISE_LINEAR_PLASTICITY")
		} else {
			head = []byte("*MAT_PIECEWISE_LINEAR_PLASTICITY_TITLE")
		}
		if err := d.dumpLine(string(head)); err != nil {
			return err
		}
		if e.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, e); err != nil {
				return err
			}
		}
		if err := d.dumpLine("$      mid        ro         e        pr      sigy      etan      fail      tdel"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matPiecewiseLinearPlasticityCards[0], 1, e); err != nil {
			break
		}
		if err := d.dumpLine("$        c         p      lcss      lcsr        vp       lcf"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matPiecewiseLinearPlasticityCards[1], 9, e); err != nil {
			break
		}
		if err := d.dumpLine("$     eps1      eps2      eps3      eps4      eps5      eps6      eps7      eps8"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matPiecewiseLinearPlasticityCards[2], 15, e); err != nil {
			break
		}
		if err := d.dumpLine("$      es1       es2       es3       es4       es5       es6       es7       es8"); err != nil {
			return err
		}
		if err = d.dumpCardFromStruct(matPiecewiseLinearPlasticityCards[3], 16, e); err != nil {
			break
		}
	}
	return err
}

func (l *MatPiecewiseLinearPlasticityList) clear() {
	*l = nil
}
