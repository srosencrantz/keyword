/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
	"errors"
	"strconv"
	"unsafe"
)

// This is a performance hack to deal with the fact that the strconv
// functions don't take byte slices and force string conversions.
func unsafeString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// Splits a field of the designated width from line.
// Returns the field and the remaining data in the line.
func splitField(line []byte, width int) ([]byte, []byte) {
	l := len(line)
	if l < width {
		width = l
	}
	return bytes.TrimSpace(line[:width]), line[width:]
}

// A field is a single column within a single row (card) of
// a keyword file.  There are different fields for different
// datatypes and widths.
type field interface {
	Parse(line []byte, value interface{}) ([]byte, error)
	Dump(line []byte, value interface{}) ([]byte, error)
}

func intDflt(x int64) *int64 {
	return &x
}

func floatDflt(x float64) *float64 {
	return &x
}

type intF struct {
	width int
	dflt  *int64
}

func (f intF) Parse(line []byte, value interface{}) ([]byte, error) {
	var err error
	cur, rest := splitField(line, f.width)
	v := value.(*int64)
	if len(cur) > 0 {
		*v, err = strconv.ParseInt(unsafeString(cur), 10, 64)
	} else if f.dflt != nil {
		*v = *f.dflt
	}
	return rest, err
}
func (f intF) Dump(line []byte, value interface{}) ([]byte, error) {
	var str string
	v := value.(int64)
	if f.dflt == nil || v != *f.dflt {
		str = strconv.FormatInt(value.(int64), 10)
	}
	l := len(str)
	if l > f.width {
		return line, errors.New("Integer too large for specified field width")
	}
	for i := l; i < f.width; i++ {
		line = append(line, ' ')
	}
	return append(line, str...), nil
}

type arrIntF intF

func (f arrIntF) Parse(line []byte, value interface{}) ([]byte, error) {
	var err error
	v := value.([]int64)
	for i := range v {
		if line, err = intF(f).Parse(line, &v[i]); err != nil {
			break
		}
	}
	return line, err
}
func (f arrIntF) Dump(line []byte, value interface{}) ([]byte, error) {
	var err error
	v := value.([]int64)
	for i := range v {
		if line, err = intF(f).Dump(line, v[i]); err != nil {
			break
		}
	}
	return line, err
}

type floatF struct {
	width int
	dflt  *float64
}

func (f floatF) Parse(line []byte, value interface{}) ([]byte, error) {
	var err error
	cur, rest := splitField(line, f.width)
	v := value.(*float64)
	if len(cur) > 0 {
		*v, err = strconv.ParseFloat(unsafeString(cur), 64)
	} else if f.dflt != nil {
		*v = *f.dflt
	}
	return rest, err
}
func (f floatF) Dump(line []byte, value interface{}) ([]byte, error) {
	var str string
	v := value.(float64)
	if f.dflt == nil || v != *f.dflt {
		str = strconv.FormatFloat(value.(float64), 'E', f.width-7, 64)
	}
	l := len(str)
	if l > f.width {
		return line, errors.New("Float too large for specified field width")
	}
	for i := l; i < f.width; i++ {
		line = append(line, ' ')
	}
	return append(line, str...), nil
}

type arrFloatF floatF

func (f arrFloatF) Parse(line []byte, value interface{}) ([]byte, error) {
	var err error
	v := value.([]float64)
	for i := range v {
		if line, err = floatF(f).Parse(line, &v[i]); err != nil {
			break
		}
	}
	return line, err
}
func (f arrFloatF) Dump(line []byte, value interface{}) ([]byte, error) {
	var err error
	v := value.([]float64)
	for i := range v {
		if line, err = floatF(f).Dump(line, v[i]); err != nil {
			break
		}
	}
	return line, err
}

type stringF struct {
	width int
}

func (f stringF) Parse(line []byte, value interface{}) ([]byte, error) {
	cur, rest := splitField(line, f.width)
	v := value.(*string)
	*v = string(cur)
	return rest, nil
}
func (f stringF) Dump(line []byte, value interface{}) ([]byte, error) {
	// TODO: we should check the length here.
	l := len(value.(string))
	if l > f.width {
		return line, errors.New("String too large for specified field width")
	}
	line = append(line, value.(string)...)
	for i := l; i < f.width; i++ {
		line = append(line, ' ')
	}
	return line, nil
}
