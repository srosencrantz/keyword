/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"reflect"
)

// Write a KeywordStruct to w (in the standard keyword format).
func Dump(w io.Writer, kwds *KeywordFile) error {
	buf := bufio.NewWriter(w)
	d := (*dumper)(buf)
	v := reflect.ValueOf(kwds).Elem()
	for i, end := 0, v.NumField(); i < end; i++ {
		kwd := v.Field(i)
		if kwd.Len() == 0 {
			continue
		}
		if err := kwd.Addr().Interface().(keywordContainer).dumpKeyword(d); err != nil {
			return err
		}
	}
	buf.Flush()
	return nil
}

type dumper bufio.Writer

func (d *dumper) dump(s []byte) error {
	_, err := (*bufio.Writer)(d).Write(s)
	return err
}

func (d *dumper) dumpLine(s string) error {
	_, err := (*bufio.Writer)(d).WriteString(s)
	if err == nil {
		err = (*bufio.Writer)(d).WriteByte('\n')
	}
	return err
}

func (d *dumper) dumpCard(c card, args ...interface{}) error {
	if len(args) != len(c) {
		return fmt.Errorf("Wrong number of args for card, got %d, expected %d", len(args), len(c))
	}
	var err error
	line := make([]byte, 0, 81)
	for i := range c {
		if line, err = c[i].Dump(line, args[i]); err != nil {
			return err
		}
	}
	line = bytes.TrimRight(line, " ")
	line = append(line, '\n')
	return d.dump(line)
}

func (d *dumper) dumpCards(cards []card, args ...interface{}) error {
	start := 0
	for _, c := range cards {
		l := len(c)
		if err := d.dumpCard(c, args[start:start+l]...); err != nil {
			return err
		}
		start += l
	}
	return nil
}

func (d *dumper) dumpCardFromStruct(c card, start int, args interface{}) error {
	v := reflect.ValueOf(args).Elem()
	if nField := v.NumField(); nField-start < len(c) {
		return fmt.Errorf("Struct has too few fields, got %d < %d", nField-start, len(c))
	}
	var err error
	line := make([]byte, 0, 81)
	for i := range c {
		fld := v.Field(i + start)
		if fld.Kind() == reflect.Array {
			fld = fld.Slice(0, fld.Len())
		}
		if line, err = c[i].Dump(line, fld.Interface()); err != nil {
			return err
		}
	}
	line = bytes.TrimRight(line, " ")
	line = append(line, '\n')
	return d.dump(line)
}

func (d *dumper) dumpCardsFromStruct(cards []card, start int, args interface{}) error {
	for _, c := range cards {
		l := len(c)
		if err := d.dumpCardFromStruct(c, start, args); err != nil {
			return err
		}
		start += l
	}
	return nil
}
