/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
)

var (
	unexpectedKeyword = errors.New("Unexpected keyword transition.")
)

// Parse a KeywordFile from r.
func Parse(r io.Reader, verbose bool) (*KeywordFile, error) {
	p := &parser{0, bufio.NewReader(r)}
	return p.parse(verbose)
}

func ReadKeywordFile(filename string) (*KeywordFile, error) {
	return ReadKeywordFileB(filename, true)
}

func ReadKeywordFileB(filename string, verbose bool) (*KeywordFile, error) {
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return nil, errors.New(fmt.Sprint("Couldn't open keyword file: ", err))
	}
	kwd, err2 := Parse(file, verbose)
	if err2 != nil {
		return nil, errors.New(fmt.Sprint("Couldn't parse keyword file: ", err2))
	}
	return kwd, nil
}

func WriteKeywordFile(kwd *KeywordFile, filename string) error {
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		return errors.New(fmt.Sprint("Couldn't Create keyword file: ", err))
	}
	err2 := Dump(bufio.NewWriter(f), kwd)
	if err2 != nil {
		return errors.New(fmt.Sprint("Couldn't Write keyword file: ", err2))
	}
	return nil
}

type parser struct {
	line int
	r    *bufio.Reader
}

// Given a keyword header line "*KEYWORD" and a keyword struct, find
// the keywordContainer (usually a slice of structs) for the
// relevant keyword.
func (p *parser) findContainer(head []byte, kwds *KeywordFile) keywordContainer {
	// Trim the '*' and the newline at the end of the header line.
	end := len(head)
	if head[end-1] == '\n' {
		end--
	}
	head = head[1:end]

	// Cut off after the first space, everything after the first space
	// seems to be ignored, and is not part of the keyword.
	if idx := bytes.IndexByte(head, ' '); idx != -1 {
		head = head[:idx]
	}

	// Title case the whole thing.  This copies.
	head = bytes.Replace(head, []byte{'_'}, []byte{' '}, -1)
	head = bytes.Title(bytes.ToLower(head))

	// Now we look for the key in the keyword file struct.  If we don't
	// find it, we remove the last word of the keyword name and look
	// again. This way people can have a function that parses all the
	// different options for a keyword if they want.
	fileValue := reflect.ValueOf(kwds).Elem()
	for {
		// We eliminate the underscores because go variables are CamelCase.
		name := string(bytes.Replace(head, []byte{' '}, []byte{}, -1))
		if containerValue := fileValue.FieldByName(name); containerValue.IsValid() {
			return containerValue.Addr().Interface().(keywordContainer)
		}
		idx := bytes.LastIndex(head, []byte{' '})
		if idx == -1 {
			break
		}
		head = head[:idx]
	}
	return &kwds.Unknown
}

func (p *parser) parse(verbose bool) (*KeywordFile, error) {
	var err error
	var b byte
	var head []byte
	var container keywordContainer
	k := &KeywordFile{}
	lasthead := []byte("none")
	for {
		if b, err = p.r.ReadByte(); err != nil {
			break
		}
		if err = p.r.UnreadByte(); err != nil {
			break
		}
		if b == '$' {
			if head, err = p.getLine(); err != nil {
				break
			}
			continue
		}
		if b == '*' {
			if head, err = p.getLine(); err != nil {
				if len(head) == 0 || err != io.EOF {
					break
				}
			}
			container = p.findContainer(head, k)
		}
		if container == nil {
			err = errors.New("Expecting a keyword introduction.")
		}
		if !bytes.Equal(lasthead, head) {
			if verbose {
				log.Print("Parsing keyword: ", string(head))
			}
			lasthead = head
		}
		if err = container.parseElement(head, p); err != nil {
			break
		}
	}
	if err == io.EOF {
		err = nil
	}
	if err != nil {
		err = fmt.Errorf("error line %d: %s", p.line, err)
	}
	return k, err
}

func (p *parser) getLine() ([]byte, error) {
	p.line++
	return p.r.ReadBytes('\n')
}

func (p *parser) getNonCommentLine() (line []byte, err error) {
	for {
		line, err = p.getLine()
		if err != nil || len(line) == 0 || line[0] != '$' {
			break
		}
	}
	return
}

func (p *parser) getInKeywordLine() (line []byte, err error) {
	for {
		var b byte
		b, err = p.r.ReadByte()
		if err != nil {
			return nil, err
		}
		err = p.r.UnreadByte()
		if err != nil {
			return nil, err
		}
		if b == '*' {
			return nil, unexpectedKeyword
		}

		line, err = p.getLine()
		if err != nil || len(line) == 0 || line[0] != '$' {
			break
		}
	}
	return
}

func (p *parser) parseCard(c card, args ...interface{}) error {
	line, err := p.getInKeywordLine()
	if err != nil {
		return err
	}
	if len(args) != len(c) {
		return fmt.Errorf("Wrong number of args for card, got %d, expected %d", len(args), len(c))
	}
	for i := range c {
		if line, err = c[i].Parse(line, args[i]); err != nil {
			break
		}
	}
	return err
}

func (p *parser) parseCards(cards []card, args ...interface{}) error {
	start := 0
	for _, c := range cards {
		l := len(c)
		if err := p.parseCard(c, args[start:start+l]...); err != nil {
			return err
		}
		start += l
	}
	return nil
}

// args must be a pointer to struct.
func (p *parser) parseCardToStruct(c card, start int, args interface{}) error {
	line, err := p.getInKeywordLine()
	if err != nil {
		return err
	}
	v := reflect.ValueOf(args).Elem()
	if nField := v.NumField(); nField-start < len(c) {
		return fmt.Errorf("Struct has too few fields, got %d < %d", nField-start, len(c))
	}
	for i := range c {
		fld := v.Field(i + start)
		if fld.Kind() == reflect.Array {
			fld = fld.Slice(0, fld.Len())
		} else {
			fld = fld.Addr()
		}
		if line, err = c[i].Parse(line, fld.Interface()); err != nil {
			break
		}
	}
	return err
}

func (p *parser) parseCardsToStruct(cards []card, start int, args interface{}) error {
	for _, c := range cards {
		l := len(c)
		if err := p.parseCardToStruct(c, start, args); err != nil {
			return err
		}
		start += l
	}
	return nil
}

func (p *parser) parseOptionalCard(c card, args ...interface{}) error {
	if err := p.parseCard(c, args); err != nil && err != unexpectedKeyword {
		return err
	}
	return nil
}
