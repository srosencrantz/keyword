/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestNode(t *testing.T) {
	var cases = []string{
		`*NODE
$    nid               x               y               z      tc      rc
    1001 1.301030000E+01-1.138254000E+00 5.350000000E+01 2.0E+00 0.0E+00
    1002 1.299378000E+01 1.263764000E+00 5.350000000E+01 0.0E+00 3.0E+00
    1003 1.299378000E+01 1.263764000E+00 5.113044000E+01 0.0E+00 0.0E+00
    1004 1.301030000E+01-1.138254000E+00 5.113044000E+01 0.0E+00 0.0E+00
    1005 1.299378000E+01 1.263764000E+00 4.876087000E+01 0.0E+00 0.0E+00
`}

	CheckCases(cases, t)
}
