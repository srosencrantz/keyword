/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestMatPiecewiseLinearPlasticity(t *testing.T) {
	var cases = []string{
		`*MAT_PIECEWISE_LINEAR_PLASTICITY
$      mid        ro         e        pr      sigy      etan      fail      tdel
         1 7.200E-06 9.000E+12 3.000E-01 1.000E+06           3.000E-01 1.000E-07
$        c         p      lcss      lcsr        vp       lcf

$     eps1      eps2      eps3      eps4      eps5      eps6      eps7      eps8
 1.000E+00 2.000E+00 3.000E+00 4.000E+00 5.000E+00 6.000E+00 7.000E+00 8.000E+00
$      es1       es2       es3       es4       es5       es6       es7       es8
 9.000E+00 1.000E+01 1.100E+01 1.200E+01 1.300E+01 1.400E+01 1.500E+01 1.600E+01
*MAT_PIECEWISE_LINEAR_PLASTICITY_TITLE
This is the title for a mat piecewise linear plasticity card
$      mid        ro         e        pr      sigy      etan      fail      tdel
         1 7.200E-06 9.000E+12 3.000E-01 1.000E+06           3.000E-01 1.000E-07
$        c         p      lcss      lcsr        vp       lcf

$     eps1      eps2      eps3      eps4      eps5      eps6      eps7      eps8
 1.000E+00 2.000E+00 3.000E+00 4.000E+00 5.000E+00 6.000E+00 7.000E+00 8.000E+00
$      es1       es2       es3       es4       es5       es6       es7       es8
 9.000E+00 1.000E+01 1.100E+01 1.200E+01 1.300E+01 1.400E+01 1.500E+01 1.600E+01
`}
	CheckCases(cases, t)
}
