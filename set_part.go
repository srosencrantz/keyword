/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type SetPart struct {
	Title             string
	Sid               int64
	Da1               float64
	Da2               float64
	Da3               float64
	Da4               float64
	Solv              string
	BlankLines        []*SetPartListLine
	ListLines         []*SetPartListLine
	ColumnLines       []*SetPartColumnLine
	ListGenerateLines []*SetPartListLine
}

var setPartCard = card{i10, f10d, f10d, f10d, f10d, stringF{10}}

type SetPartListLine struct {
	P [8]int64
}

var setPartListLineCard = card{arrIntF{10, intDflt(0)}}

type SetPartColumnLine struct {
	Pid int64
	A   [4]float64
}

var setPartColumnLineCard = card{i10, arrFloatF{10, floatDflt(0.0)}}

type SetPartList []*SetPart

func (l *SetPartList) parseElement(head []byte, p *parser) error {
	n := &SetPart{}
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, n); err != nil {
			return err
		}
	}
	if err := p.parseCardToStruct(setPartCard, 1, n); err != nil {
		return err
	}
	switch {
	case bytes.Contains(head, []byte("_COLUMN")):
		n.ColumnLines = make([]*SetPartColumnLine, 0)
		for {
			m := &SetPartColumnLine{}
			if err := p.parseCardToStruct(setPartColumnLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ColumnLines = append(n.ColumnLines, m)
		}
	case bytes.Contains(head, []byte("_GENERATE")):
		n.ListGenerateLines = make([]*SetPartListLine, 0)
		for {
			m := &SetPartListLine{}
			if err := p.parseCardToStruct(setPartListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListGenerateLines = append(n.ListGenerateLines, m)
		}
	case bytes.Contains(head, []byte("_LIST")):
		n.ListLines = make([]*SetPartListLine, 0)
		for {
			m := &SetPartListLine{}
			if err := p.parseCardToStruct(setPartListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListLines = append(n.ListLines, m)
		}
	default:
		n.BlankLines = make([]*SetPartListLine, 0)
		for {
			m := &SetPartListLine{}
			if err := p.parseCardToStruct(setPartListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.BlankLines = append(n.BlankLines, m)
		}
	}

}

func (l *SetPartList) dumpKeyword(d *dumper) error {
	for _, n := range *l {
		head := []byte("*SET_PART")
		switch {
		case n.ColumnLines != nil:
			head = append(head, []byte("_COLUMN")...)
		case n.ListGenerateLines != nil:
			head = append(head, []byte("_LIST_GENERATE")...)
		case n.ListLines != nil:
			head = append(head, []byte("_LIST")...)
		}
		if n.Title != "" {
			head = append(head, []byte("_TITLE")...)
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}

		if n.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, n); err != nil {
				return err
			}
		}

		if err := d.dumpLine("$      sid       da1       da2       da3       da4      solv"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(setPartCard, 1, n); err != nil {
			return err
		}

		switch {
		case n.ColumnLines != nil:
			if err := d.dumpLine("$      pid        a1        a2        a3        a4"); err != nil {
				return err
			}
			for _, m := range n.ColumnLines {
				if err := d.dumpCardFromStruct(setPartColumnLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.ListGenerateLines != nil:
			if err := d.dumpLine("$    b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end"); err != nil {
				return err
			}
			for _, m := range n.ListGenerateLines {
				if err := d.dumpCardFromStruct(setPartListLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.ListLines != nil:
			if err := d.dumpLine("$     pid1      pid2      pid3      pid4      pid5      pid6      pid7      pid8"); err != nil {
				return err
			}
			for _, m := range n.ListLines {
				if err := d.dumpCardFromStruct(setPartListLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.BlankLines != nil:
			if err := d.dumpLine("$     pid1      pid2      pid3      pid4      pid5      pid6      pid7      pid8"); err != nil {
				return err
			}
			for _, m := range n.BlankLines {
				if err := d.dumpCardFromStruct(setPartListLineCard, 0, m); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (l *SetPartList) clear() {
	*l = nil
}
