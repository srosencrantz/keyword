/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestConstrainedGeneralizedWeld(t *testing.T) {
	var cases = []string{
		`*CONSTRAINED_GENERALIZED_WELD_SPOT_ID
$      wid
  55004447
$     nsid       cid    filter    window       npr      nprt
  50006069         0         0 0.000E+00         0         0
$    tfail      epsf        sn        ss         n         m
 0.000E+00&w1ep     &w1sn     &w1ss     &w1n      &w1m
*CONSTRAINED_GENERALIZED_WELD_SPOT_ID
  55004448
  50006070         0         0 0.000E+00         0         0
 0.000E+00&w1ep     &w1sn     &w1ss     &w1n      &w1m
*CONSTRAINED_GENERALIZED_WELD_SPOT
  50006071         0         0 0.000E+00         0         0
 0.000E+00&w1ep     &w1sn     &w1ss     &w1n      &w1m
`}
	CheckCases(cases, t)
}
