/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import "bytes"

type ElementMass struct {
	Eid     int64
	Id      int64
	Mass    float64
	Pid     int64
	NodeSet bool
}

var elementMassCard = card{i8, i8, f16d, i8}

type ElementMassList []*ElementMass

func (l *ElementMassList) parseElement(head []byte, p *parser) error {
	e := &ElementMass{}
	*l = append(*l, e)
	if bytes.Contains(head, []byte("_NODE_SET")) {
		e.NodeSet = true
	} else {
		e.NodeSet = false
	}
	err := p.parseCardToStruct(elementMassCard, 0, e)
	return err
}

func (l *ElementMassList) dumpKeyword(d *dumper) error {
	var err error
	first := true
	current := true
	for _, e := range *l {
		if first || current != e.NodeSet {
			current = e.NodeSet
			if e.NodeSet {
				if err := d.dumpLine("*ELEMENT_MASS_NODE_SET"); err != nil {
					return err
				}
			} else {
				if err := d.dumpLine("*ELEMENT_MASS"); err != nil {
					return err
				}
			}
			if err := d.dumpLine("$    eid      id            mass     pid"); err != nil {
				return err
			}
		}
		first = false
		if err = d.dumpCardFromStruct(elementMassCard, 0, e); err != nil {
			break
		}
	}
	return err
}

func (l *ElementMassList) clear() {
	*l = nil
}
