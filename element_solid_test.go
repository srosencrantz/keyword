/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestElementSolid(t *testing.T) {
	var cases = []string{
		`*ELEMENT_SOLID
$    eid     pid      n1      n2      n3      n4      n5      n6      n7      n8
       1    5242       1       2     161     160   17332   17333   17492   17491
       2    5242       2       3     162     161   17333   17334   17493   17492
       3    5242       3       4     163     162   17334   17335   17494   17493
       4    5242       4       5     164     163   17335   17336   17495   17494
       5    5242       5       6     165     164   17336   17337   17496   17495
       6    5242       6       7     166     165   17337   17338   17497   17496
       7    5242       7       8     167     166   17338   17339   17498   17497
`}
	CheckCases(cases, t)
}
