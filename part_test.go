/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"testing"
)

func TestPart(t *testing.T) {
	var cases = []string{
		`*PART
$ title
Rib 0
$      pid     secid       mid     eosid      hgid      grav    adpopt      tmid
         1         1       114
`,
		`*PART_INERTIA
$ title
material type # (rigid)
$      pid     secid       mid     eosid      hgid      grav    adpopt      tmid
         1         1         1
$       xc        yc        zc        tm      ircs      node
-1.740E-01 0.000E+00 6.520E-01 4.540E+00
$      ixx       ixy       ixz       iyy       iyz        iz
 1.590E-02 0.000E+00 1.420E-04 2.400E-02 0.000E+00 2.210E-02
$      vtx       vty       vtz       vrx       vry         v
 1.480E+01 0.000E+00 0.000E+00 0.000E+00 0.000E+00 0.000E+00
*PART_INERTIA
$ title
material type # (rigid)
$      pid     secid       mid     eosid      hgid      grav    adpopt      tmid
         2         2         2
$       xc        yc        zc        tm      ircs      node
-1.640E-01 0.000E+00 5.120E-01 1.480E+00         1
$      ixx       ixy       ixz       iyy       iyz        iz
 1.000E-02 0.000E+00 0.000E+00 1.000E-02 0.000E+00 1.000E-02
$      vtx       vty       vtz       vrx       vry         v
 1.480E+01 0.000E+00 0.000E+00 0.000E+00 0.000E+00 0.000E+00
$       xl        yl        zl      xlip      ylip      zlip       cid
 1.000E+00 2.000E+00 3.000E+00 4.000E+00 5.000E+00 6.000E+00         7
`}

	CheckCases(cases, t)
}
