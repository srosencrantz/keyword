/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package keyword

import (
	"bytes"
)

type SetShell struct {
	Title             string
	Sid               int64
	Da1               float64
	Da2               float64
	Da3               float64
	Da4               float64
	BlankLines        []*SetShellListLine
	ListLines         []*SetShellListLine
	ColumnLines       []*SetShellColumnLine
	ListGenerateLines []*SetShellListLine
	GeneralLines      []*SetShellGeneralLine
}

var setShellCard = card{i10, f10d, f10d, f10d, f10d}

type SetShellListLine struct {
	Eid [8]int64
}

var setShellListLineCard = card{arrIntF{10, intDflt(0)}}

type SetShellColumnLine struct {
	Eid int64
	A   [4]float64
}

var setShellColumnLineCard = card{i10, arrFloatF{10, floatDflt(0.0)}}

type SetShellGeneralLine struct {
	Option string
	E      [7]int64
}

var setShellGeneralLineCard = card{stringF{10}, arrIntF{10, intDflt(0)}}

type SetShellList []*SetShell

func (l *SetShellList) parseElement(head []byte, p *parser) error {
	n := &SetShell{}
	*l = append(*l, n)
	if bytes.Contains(head, []byte("_TITLE")) {
		if err := p.parseCardToStruct(title, 0, n); err != nil {
			return err
		}
	}
	if err := p.parseCardToStruct(setShellCard, 1, n); err != nil {
		return err
	}
	switch {
	case bytes.Contains(head, []byte("_COLUMN")):
		n.ColumnLines = make([]*SetShellColumnLine, 0)
		for {
			m := &SetShellColumnLine{}
			if err := p.parseCardToStruct(setShellColumnLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ColumnLines = append(n.ColumnLines, m)
		}
	case bytes.Contains(head, []byte("_GENERATE")):
		n.ListGenerateLines = make([]*SetShellListLine, 0)
		for {
			m := &SetShellListLine{}
			if err := p.parseCardToStruct(setShellListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListGenerateLines = append(n.ListGenerateLines, m)
		}
	case bytes.Contains(head, []byte("_GENERAL")):
		n.GeneralLines = make([]*SetShellGeneralLine, 0)
		for {
			m := &SetShellGeneralLine{}
			if err := p.parseCardToStruct(setShellGeneralLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.GeneralLines = append(n.GeneralLines, m)
		}
	case bytes.Contains(head, []byte("_LIST")):
		n.ListLines = make([]*SetShellListLine, 0)
		for {
			m := &SetShellListLine{}
			if err := p.parseCardToStruct(setShellListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.ListLines = append(n.ListLines, m)
		}
	default:
		n.BlankLines = make([]*SetShellListLine, 0)
		for {
			m := &SetShellListLine{}
			if err := p.parseCardToStruct(setShellListLineCard, 0, m); err != nil {
				if err == unexpectedKeyword {
					err = nil
				}
				return err
			}
			n.BlankLines = append(n.BlankLines, m)
		}
	}

}

func (l *SetShellList) dumpKeyword(d *dumper) error {
	for _, n := range *l {
		head := []byte("*SET_SHELL")
		switch {
		case n.ColumnLines != nil:
			head = append(head, []byte("_COLUMN")...)
		case n.ListGenerateLines != nil:
			head = append(head, []byte("_LIST_GENERATE")...)
		case n.GeneralLines != nil:
			head = append(head, []byte("_GENERAL")...)
		case n.ListLines != nil:
			head = append(head, []byte("_LIST")...)
		}
		if n.Title != "" {
			head = append(head, []byte("_TITLE")...)
		}

		if err := d.dumpLine(string(head)); err != nil {
			return err
		}

		if n.Title != "" {
			if err := d.dumpCardFromStruct(title, 0, n); err != nil {
				return err
			}
		}

		if err := d.dumpLine("$      sid       da1       da2       da3       da4"); err != nil {
			return err
		}
		if err := d.dumpCardFromStruct(setShellCard, 1, n); err != nil {
			return err
		}

		switch {
		case n.ColumnLines != nil:
			if err := d.dumpLine("$      eid        a1        a2        a3        a4"); err != nil {
				return err
			}
			for _, m := range n.ColumnLines {
				if err := d.dumpCardFromStruct(setShellColumnLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.ListGenerateLines != nil:
			if err := d.dumpLine("$    b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end"); err != nil {
				return err
			}
			for _, m := range n.ListGenerateLines {
				if err := d.dumpCardFromStruct(setShellListLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.GeneralLines != nil:
			if err := d.dumpLine("$   option        e1        e2        e3        e4        e5        e6        e7"); err != nil {
				return err
			}
			for _, m := range n.GeneralLines {
				if err := d.dumpCardFromStruct(setShellGeneralLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.ListLines != nil:
			if err := d.dumpLine("$     eid1      eid2      eid3      eid4      eid5      eid6      eid7      eid8"); err != nil {
				return err
			}
			for _, m := range n.ListLines {
				if err := d.dumpCardFromStruct(setShellListLineCard, 0, m); err != nil {
					return err
				}
			}
		case n.BlankLines != nil:
			if err := d.dumpLine("$     eid1      eid2      eid3      eid4      eid5      eid6      eid7      eid8"); err != nil {
				return err
			}
			for _, m := range n.BlankLines {
				if err := d.dumpCardFromStruct(setShellListLineCard, 0, m); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (l *SetShellList) clear() {
	*l = nil
}
